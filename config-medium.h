#ifndef LESTER_CONFIG_SPEC
#define LESTER_CONFIG_SPEC


// A nice propagation with an emergent arc
//  (try also N=25 for a sometimes better connected arc, and lam=1864 for thicker/broader circle)

static const int N=64; // the number of sites
const Real bigL = 1;
const Real epsilon = bigL/N;
const Real mass = 16; // silly, but needed for backwards compatibility
// int width=700, height=600;
int lumpSize=static_cast<int>(256./N);
const int globalMaxParticles=2;
#define PHIPOWER 3
const int startType = 7; // 0=random, 1=all uniform, 2=just vacuum, 3=just 0 mom mode, 4=2&3, 5=posn packet, 6=moving, 7 = 2d blob
const Real deltaBoost1D = 1;
const Real deltaBoost2D = 1;
const Real OVERALL_SCALE=1;
const Real sf0    = OVERALL_SCALE*3000;
const Real sf1    = OVERALL_SCALE*20;
const Real sf2    = OVERALL_SCALE*1;
const Real sf3    = OVERALL_SCALE*1;
const Real POSsf0 = OVERALL_SCALE*3000;
const Real POSsf1 = OVERALL_SCALE*40;
const Real POSsf2 = OVERALL_SCALE*1;
const Real POSsf3 = OVERALL_SCALE*1;
const bool showHintTerms = false;
const Real dtQFT = 0.01*1.2 / 7 /2 /10         *bigL/N; // looks silly, but there for backwards compatibility after energy unit bug found -- 
const Real tPrintInterval = 5          *bigL/N; // looks silly, but there for backwards compatibility after energy unit bug found -- 
const Real lambda = 2900; //0.2/pow(epsilon,3)*1.05     ;// 819; //0.2/pow(epsilon,3+0*(0.5*PHIPOWER+1));   // (cute if power is 3) // looks silly, but there for backwards compatibility after energy unit bug found -- 
const int maxParticleStatesToShow=3;
const bool showOnlyUnique=false;
const bool showPositionSpace=true;
const bool linearRep=false;
const bool useNewEvolCalc =  true;
const bool truncateModes = true;   // can set this to true or false -- you choose -- it might make the setup quicker, but the physics should be the same regardless.  
const int updatesBetweenDisplays = 400;
const bool useFancyCols = true;
const Real fancyColsThresh = 0.002;
const bool monochrome = true;
const bool calc1PNormAllTheTime = true; // warning ... this slows things down
	    const double BLOBsigma=1.*N/24.; // in momentum space
	    const double BLOBmeanPX=+1.*N/4;
	    const double BLOBmeanPY=-1.*N/4;
	    const double BLOBxPos = 1.*N-0.25*N;
	    const double BLOByPos = 0.25*N;
const char * nameOfDerivativeMatrixDumpFile = "DerivMat-config-medium.dat";
#endif
