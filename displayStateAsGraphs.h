
#ifndef LESTER_DISPLAYSTATEASGRAPHS_H
#define LESTER_DISPLAYSTATEASGRAPHS_H

extern "C" {
#include "ezxdisp.h"
}

#include "displayUtils.h"
#include "Coeffs.h"
#include "globals.h"
#include "VideoEncoder.h"

#define EITHER1D ((coeffs2)?(z0-zFree)*deltaBoost1D:z0)
#define EITHER2D ((coeffs2)?(z0-zFree)*deltaBoost2D:z0)

template<typename T>
void displayStateAsGraphs(Coeffs<T> & coeffs, Coeffs<T> * const coeffs2, ezx_t *e, VideoEncoder * video=0) {

  Comp zFree;
  
  ezx_wipe(e);

  const int switchy = showOnlyUnique ? 1 : 0;
  
  static const int margin = 20;
  
  static const Real desiredArea = N*lumpSize*lumpSize;
  static const int posnOffset = N*lumpSize+margin;

  int xOff=margin;
  int yOff=margin;
 
  if (maxParticleStatesToShow>=0 && globalMaxParticles>=0) {

    const int width=static_cast<int>(1*lumpSize);
      
    ModState special;
    const Comp & z0 = coeffs.slow_getCoeff(special);
    
    ezx_fillrect_2d(e, xOff, yOff, xOff+width, yOff+static_cast<int>(sf0*desiredArea*norm(z0)/width), oldPhaseCol(z0));	  
    if (showPositionSpace) {
      yOff += posnOffset;
	
      ezx_fillrect_2d(e, xOff, yOff, xOff+width, yOff+static_cast<int>(sf0*desiredArea*norm(z0)/width), oldPhaseCol(z0));	  
	
      yOff -= posnOffset;
    }

    xOff += width+margin;
  }
  
  if (maxParticleStatesToShow>=1 && globalMaxParticles>=1) {
    
    const int width=lumpSize;
    
    
    ezx_fillrect_2d(e, xOff+0*width, yOff-3, xOff+N*width, yOff-2, myBlack());     
    for (int x=0; x<N; ++x) {
      ModState special;
      special.incrementMode(x);
      const Comp & z0 = coeffs.slow_getCoeff(special);
      if (coeffs2) {
	zFree = coeffs2->slow_getCoeff(special);
      }
      
      ezx_fillrect_2d(e, xOff+x*width, yOff, xOff+(x+1)*width, yOff+static_cast<int>(sf1*desiredArea*norm(EITHER1D)/width), oldPhaseCol(EITHER1D));     
    }
    

    if (showPositionSpace) {

      coeffs.get1DPosData();

      yOff += posnOffset;

      ezx_fillrect_2d(e, xOff+0*width, yOff-3, xOff+N*width, yOff-2, myBlack());     
      for (int x=0; x<N; ++x) {
	const Comp  z0=Comp(coeffs.m_pos1D[x][0],coeffs.m_pos1D[x][1])/sqrt(static_cast<Real>(N));

      if (coeffs2) {
	zFree = Comp(coeffs2->m_pos1D[x][0],coeffs2->m_pos1D[x][1])/sqrt(static_cast<Real>(N));
      }

	if (x==N/2) {
	  const Real currentMidNorm = norm(z0);
	  if (currentMidNorm>biggestMidNorm) {
	    biggestMidNorm = currentMidNorm;
	    //std::cout << "Biggest mid norm " << biggestMidNorm << std::endl;
	  }
	}

       
	ezx_fillrect_2d(e, xOff+x*width, yOff+0, xOff+(x+1)*width, yOff+static_cast<int>(POSsf1*desiredArea*norm(EITHER1D)/width), oldPhaseCol(EITHER1D));     
      }

      yOff -= posnOffset;

    }

    xOff += N*lumpSize + margin;
  }

  if (maxParticleStatesToShow>=2 && globalMaxParticles>=2) {

    for (int x=0; x<N; ++x) {
      for (int y=switchy*x; y<N; ++y) {

	ModState special;
	special.incrementMode(x);
	special.incrementMode(y);
	const Comp & z0 = coeffs.slow_getCoeff(special);
	if (coeffs2) {
	  zFree = coeffs2->slow_getCoeff(special);
	}

	if (useFancyCols) {

	  ezx_fillrect_2d(e, 			  
			  xOff+x*lumpSize, 
			  yOff+y*lumpSize,
			  xOff+x*lumpSize+lumpSize, 
			  yOff+y*lumpSize+lumpSize,
			  phaseCol(EITHER2D));     

	} else {
	
	  const Real area = desiredArea*sf2*norm(EITHER2D);
	  const Real halfWid = 0.5*sqrt(area);
	  
	  ezx_fillrect_2d(e, 			  
			  xOff+static_cast<int>((x)*lumpSize-halfWid), 
			  yOff+static_cast<int>((y)*lumpSize-halfWid),
			  xOff+static_cast<int>((x)*lumpSize+halfWid), 
			  yOff+static_cast<int>((y)*lumpSize+halfWid),
			  oldPhaseCol(EITHER2D));     

	}
      }
    }


    if (showPositionSpace) {
       
      yOff += posnOffset;

      coeffs.get2DPosData();
      if (coeffs2) {
	coeffs2->get2DPosData();
      }

#ifdef LESTER_VIDEO
      if (video) {
	
	static bool first=true;
	if (first) {
	  first =false;
	  /* Cb and Cr */
	  for(int y=0;y<video->c->height/2;y++) {
	    for(int x=0;x<video->c->width/2;x++) {
	      
	      // the x and y have an origin at "top left".
	      
	      // Try to resemble http://en.wikipedia.org/wiki/YCbCr picture
	      const float ermX /*0 at leftEdge, +1 at rightEdge */ = (1.0*x/(video->c->width/2));
	      const float ermY /*0 at botEdge, +1 at topEdge */ = 1.0-(1.0*y/(video->c->height/2));
	      
	      video->picture->data[1][y * video->picture->linesize[1] + x] = 128; //static_cast<int>(ermX*256); // Cb
	      video->picture->data[2][y * video->picture->linesize[2] + x] = 128; //static_cast<int>(ermY*256); // Cr
	    }
	  }
	  
	  /* prepare a dummy image */
	  /* Y */
	  for(int y=0;y<video->c->height;y++) {
	    for(int x=0;x<video->c->width;x++) {
	      video->picture->data[0][y * video->picture->linesize[0] + x] = 128;
	    }
	  }

	}

	/* Y */
	for (int x=0; x<N; ++x) {
	  for (int y=switchy*x; y<N; ++y) {
	    
	    Comp  z0=Comp(coeffs.m_pos2D[x*N+y][0],coeffs.m_pos2D[x*N+y][1])/sqrt(static_cast<Real>(N))/sqrt(static_cast<Real>(N));
	    
	    
	    video->picture->data[0][y * video->picture->linesize[0] + x] =  static_cast<unsigned int>(lamVal(z0)*256) % 256;
	  }
	}


	/* encode the image */
	video->out_size = avcodec_encode_video(video->c, video->outbuf, video->outbuf_size, video->picture);
	video->had_output |= video->out_size;
	printf("encoding frame %3d (size=%5d)\n", video->i, video->out_size);
	fwrite(video->outbuf, 1, video->out_size, video->f);
	
	
      }
#endif
 
      for (int x=0; x<N; ++x) {
	for (int y=switchy*x; y<N; ++y) {

	  Comp  z0=Comp(coeffs.m_pos2D[x*N+y][0],coeffs.m_pos2D[x*N+y][1])/sqrt(static_cast<Real>(N))/sqrt(static_cast<Real>(N));
	  
	  if (coeffs2) {
	    zFree = Comp(coeffs2->m_pos2D[x*N+y][0],coeffs2->m_pos2D[x*N+y][1])/sqrt(static_cast<Real>(N))/sqrt(static_cast<Real>(N));
	  }
	  
	  if (useFancyCols) {
	    
	    ezx_fillrect_2d(e, 			  
			    xOff+x*lumpSize, 
			    yOff+y*lumpSize,
			    xOff+x*lumpSize+lumpSize, 
			    yOff+y*lumpSize+lumpSize,
			    phaseCol(EITHER2D));     

	  } else {

	    const Real area = desiredArea*POSsf2*norm(EITHER2D);
	    const Real halfWid = 0.5*sqrt(area);
	    
	    ezx_fillrect_2d(e, 
			    xOff+static_cast<int>((x+0.5)*lumpSize-halfWid), 
			    yOff+static_cast<int>((y+0.5)*lumpSize-halfWid),
			    xOff+static_cast<int>((x+0.5)*lumpSize+halfWid), 
			    yOff+static_cast<int>((y+0.5)*lumpSize+halfWid),
			    oldPhaseCol(EITHER2D));
	  }     
	    
	}
      }
	
      yOff -= posnOffset;


    }
      



    xOff += N*lumpSize+margin;
  }

  // SHOW THREE PARTICLE STATES
  if (maxParticleStatesToShow>=3 && globalMaxParticles>=3) {
    const int smallLump = static_cast<int>(lumpSize/N);
    for (int x=0; x<N; ++x) {
      for (int y=switchy*x; y<N; ++y) {
	for (int z=switchy*y; z<N; ++z) {
	  
	  ModState special;
	  special.incrementMode(x);
	  special.incrementMode(y);
	  special.incrementMode(z);
	  const Comp & z0 = coeffs.slow_getCoeff(special);
	  
	  const Real area = desiredArea*sf3*norm(z0);
	  const Real halfWid = 0.5*sqrt(area);

	  const Real xProj[] = {0.2*lumpSize, static_cast<Real>(lumpSize), 0};
	  const Real yProj[] = {0.1*lumpSize, 0, static_cast<Real>(lumpSize)};

	  const Real xx = (x*xProj[0] + y*xProj[1] + z*xProj[2]);
	  const Real yy = (x*yProj[0] + y*yProj[1] + z*yProj[2]);

	  ezx_fillrect_2d(e,
			  static_cast<int>(xOff + xx - halfWid),
			  static_cast<int>(yOff + yy - halfWid),
			  static_cast<int>(xOff + xx + halfWid),
			  static_cast<int>(yOff + yy + halfWid),
			  oldPhaseCol(z0));
	  //std::cout << real(z0) << std::endl;
	}
      }
    }
    xOff += (N+1)*(lumpSize+smallLump);
  }
    
  {
    const int width = (int)sqrt(desiredArea*OVERALL_SCALE*coeffs.m_absError);
    ezx_fillrect_2d(e, 
		    1, 
		    1,
		    1+width,
		    1+width,
		    &ezx_black);   
      

      
  }
    
  ezx_redraw(e);


    
}

#endif

