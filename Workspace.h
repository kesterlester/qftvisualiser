
#ifndef LESTER_WORKSPACE_H
#define LESTER_WORKSPACE_H

#include "States.h"
#include "MixedState.h"

#include <vector>
#include <map>

// The workspace keeps track of things like "where the first index of any n-particle state is, and things like that.  It also owns maps that can be used to map a ModState or OccState to a standard index.  Ideally the maps would be replaced in the long run by something that simply "computes" the index from the object without the cost of storage and search associated with a map.  But I can't see an obvious "nice" way of doing that yet.
class Workspace {
public:
  typedef std::map<const ModState, int> ModStateMap;
  //typedef std::map<const OccState, int> OccStateMap;
  typedef std::map<const MixedState, int> MixStateMap;
 public:
  static Workspace & getInstance() {
    static Workspace workspace;
    return workspace;
  }
  static int beginIndexForNumParticles(const int numParticles) {
    return getInstance().m_begin[numParticles];
  }
  // Note that, like "usual" iterators, this "end" index is actually "one past" the last elt
  static int endIndexForNumParticles(const int numParticles) {
    return getInstance().m_begin[numParticles+1];
  }
  static int size() {
    return getInstance().endIndexForNumParticles(globalMaxParticles);
  }
 private:
  static const bool SETUP_MAPS = false;
  Workspace() : m_begin(globalMaxParticles+2) // the "+2" is since "+1" would be necesasary to store the g+1 numbers 0,1,2,3...,g, and an additional "1" is needed to facilitate the m_end functionality that goes beyond m_begin
    { 
      std::cout << "Initialising workspace ... " << std::flush;
      ModState ms;
      //OccState os;
      MixedState mixedState;
      //ms.setVacuum();
      //os.setVacuum();
      mixedState.setVacuum();
      int currentIndex=0;
      m_modStateToIndexMap.insert(std::pair<const ModState,int>(ms,currentIndex));
      //m_occStateToIndexMap.insert(std::pair<const OccState,int>(os,currentIndex));
      m_mixStateToIndexMap.insert(std::pair<const MixedState,int>(mixedState,currentIndex));
      for (int stateMultiplicityToSearchFor=0;
	   stateMultiplicityToSearchFor<=globalMaxParticles+1;
	   ++stateMultiplicityToSearchFor ) // The "+1" is for the m_end functionality
	{
	  while (mixedState.getNumParticles() < stateMultiplicityToSearchFor) {
	    if (SETUP_MAPS) {
	      ++ms;
	      //++os;
	    }
	    ++mixedState; //dying here.
	    ++currentIndex;
	    if (SETUP_MAPS) {
	      m_modStateToIndexMap.insert(std::pair<ModState,int>(ms,currentIndex));
	      //m_occStateToIndexMap.insert(std::pair<OccState,int>(os,currentIndex));
	      m_mixStateToIndexMap.insert(std::pair<MixedState,int>(mixedState,currentIndex));
	    }
	  }
	  // at this point, ms is the first state with "stateMultiplicityToSearchFor" particles.
	  m_begin[stateMultiplicityToSearchFor]=currentIndex;
	}
      std::cout << "done." << std::endl;
    }
 public:
  static int mixStateToIndex(const MixedState & m) {
    const bool useMap=false;
    if (useMap) {
      const Workspace::MixStateMap & theMap = Workspace::mixStateToIndexMap();
      const Workspace::MixStateMap::const_iterator it = theMap.find(m);
      assert(it!=theMap.end());
      return it->second;
    } else {
      return m.getIndex();
    }
  }
  static int modStateToIndex(const ModState & m) {
    const bool useMap=false;
    if (useMap) {
      const Workspace::ModStateMap & theMap = Workspace::modStateToIndexMap();
      const Workspace::ModStateMap::const_iterator it = theMap.find(m);
      assert(it!=theMap.end());
      return it->second;
    } else {
      return m.getIndex();
    }
  }
 private:
  static const ModStateMap & modStateToIndexMap() {
    return getInstance().m_modStateToIndexMap;
  }
  //static const OccStateMap & occStateToIndexMap() {
  //  return getInstance().m_occStateToIndexMap;
  //}
 private:
  static const MixStateMap & mixStateToIndexMap() {
    return getInstance().m_mixStateToIndexMap;
  }
 private:
  // These should be thought of as a bit like iterators (almost) in that
  // m_begin[4] will hold the "index" of the first four particle state.
  // We could have an m_end[4] as well, except that it will be equal to m_begin[5], so
  // we don't need two:
  std::vector<int> m_begin;
  ModStateMap m_modStateToIndexMap;
  //OccStateMap m_occStateToIndexMap;
  MixStateMap m_mixStateToIndexMap;
};

std::ostream & operator<<(std::ostream & os, const Workspace & ws) {
  os << "Workspace[";
  for (int i=0; i<=globalMaxParticles; ++i) {
    os << "{n="<<i<<":[" << ws.beginIndexForNumParticles(i) <<  "," <<  ws.endIndexForNumParticles(i) << ")},";
  }
  return os<< "]";
}

#endif
