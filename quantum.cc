#define NDEBUG

#include "Types.h"

#include <map>
#include <set>
#include <vector>
#include <iostream>
#include <cassert>
#include <list>
#include <cmath>
#include <fstream>
#include <unistd.h> // for usleep

#ifdef WIN32
#include "winlib.h"
#endif

#include "config.h"

#include "Workspace.h"

#include "utils.h"
#include "EnergyCache.h"


#include "Coeffs.h"


// #include "displayStateAsParticles.h"
#include "displayStateAsGraphs.h"

#include "DerivativeMatrix.h"

#include "VideoEncoder.h"


int main(int argc, char *argv[])
{
  std::cout << "HELLO " << __FILE__  << " " <<__LINE__ << std::endl;
#ifdef OVERRIDE_WIDTH
  width = OVERRIDE_WIDTH;
#endif
#ifdef OVERRIDE_HEIGHT
  height = OVERRIDE_HEIGHT;
#endif
  int maxVideoFrames = 0;
  bool saveMatrices = false;
  bool noEvolution = false;
#ifdef SHOW_DELTA
  bool computeFree = true;
  bool showDelta = true;
  bool showFree = true;
#else
  bool computeFree = false;
  bool showDelta = false;
  bool showFree = false;
#endif
  for (int i=0; i<argc; ++i) {
    if (argv[i] == std::string("save")) {
      saveMatrices = true;
      continue;
    }
    if (argv[i] == std::string("video")) {
      ++i;
      if (i<argc) {
	std::istringstream is(argv[i]);
	is >> maxVideoFrames;
	// Cant output negative number of frames
	if (maxVideoFrames<0) {
	  maxVideoFrames=0;
	}
	// Can't output stuff that is too big:
	if (N>288) {
	  std::cerr << "Not saving video as N too big" << std::endl;
	  maxVideoFrames=0;
	}
      }
      continue;
      
    }
    if (argv[i] == std::string("noEvolution")) {
      noEvolution = true;
      continue;
    }
    if (argv[i] == std::string("showFree")) {
      showFree = true;
      computeFree = true;
      continue;
    }
    if (argv[i] == std::string("showDelta")) {
      showDelta = true;
      computeFree = true;
      continue;
    }
  }


  std::cout << "HELLO " << __FILE__  << " " <<__LINE__ << std::endl;
  const bool makeVideo = maxVideoFrames>0;

  Workspace::getInstance();
  std::cout << "HELLO " << __FILE__  << " " <<__LINE__ << std::endl;

  std::cout << "Preparing initial state ... " << std::endl;
  Ket<ModState> initialKet(startType);
  std::cout << " ... initial state prepared." << std::endl;

  std::cout << "Preparing full derivative matrix ... " << std::endl;
  DerivativeMatrix derivativeMatrixFull;
  derivativeMatrixFull.setup<OccState>();
  std::cout << " ... full derivative matrix prepared." << std::endl;

  DerivativeMatrix derivativeMatrixFree;
  if (computeFree) {
    std::cout << "Preparing free derivative matrix ... " << std::endl;
    derivativeMatrixFree.setup<OccState>(true);
    std::cout << " ... free derivative matrix prepared." << std::endl;
  }

  /*
    std::cout << "Preparing another full derivative matrix ... " << std::endl;
    DerivativeMatrix derivativeMatrixAnotherFull;
    derivativeMatrixAnotherFull.setup<OccState>();
    std::cout << " ... full derivative matrix prepared." << std::endl;
  */

  std::cout << "Sorting derivative matrices ... " << std::endl;
  derivativeMatrixFull.sortMe();
  if (computeFree) derivativeMatrixFree.sortMe();
  //  derivativeMatrixAnotherFull.sortMe();
  std::cout << " ... derivative matrix sorted." << std::endl;

  if (saveMatrices) {
    std::cout << "Saving derivative matrix ... " << std::endl;
    std::ofstream file;

    file.open(nameOfDerivativeMatrixDumpFile);
    derivativeMatrixFull.dumpTo(file);
    file.close();

    if (computeFree) {
      file.open("2.dat");
      derivativeMatrixFree.dumpTo(file);
      file.close();
    }

    /*
      file.open("3.dat");
      derivativeMatrixAnotherFull.dumpTo(file);
      file.close();
    */
    std::cout << " ... derivative matrix saved." << std::endl;
  }



  std::cout << "Preparing movement states ... " << std::endl;
  Coeffs<ModState> coeffsFull(initialKet, derivativeMatrixFull);
  Coeffs<ModState> coeffsFree(initialKet, (computeFree ? derivativeMatrixFree: derivativeMatrixFull));
  std::cout << " ... movement states prepared." << std::endl;

  ezx_t *e=0;
  ezx_t *eDelta=0;
  ezx_t *eFree=0;


  if (argc >= 2 && atoi(argv[1]) != 0) srand(atoi(argv[1]));
  else srand(time(NULL));
  // if (argc >= 3) maxgen = atoi(argv[2]);

  e = ezx_init(width, height, "Lester's v2 Game of Life (Interacting Theory)");
  if (showDelta) {
    eDelta = ezx_init(width, height, "Lester's Game of Life (Delta Window)");
  }
  if (showFree) {
    eFree = ezx_init(width, height, "Lester's Game of Life (Free Theory)");
  }

  VideoEncoder * v=0;
#ifdef LESTER_VIDEO
  if (makeVideo) {
    remove("/tmp/vid.mpg");
    v = new VideoEncoder("/tmp/vid.mpg");
    if (v) {
      v->start();
    }
  }
  
#endif

  for (int gen = 0; !noEvolution; gen++) {


#ifdef LESTER_VIDEO
    if (makeVideo) {
      if (gen==maxVideoFrames) {
	// stop the video writing:
	if (v) {
	  v->stop();
	  std::cout << "Wrote video to file [" << v->filename << "]\nConsider playing with\n\nmplayer " << v->filename << "\n\n" << std::flush;
	  return 0; // quit
	}
      }
    }
#endif



    if (linearRep && false) {
      ezx_wipe(e);
      //displayStateAsParticles(coeffs, e);
      ezx_redraw(e);
    } else {
      displayStateAsGraphs(coeffsFull, ((Coeffs<ModState>*)0), e, (gen<maxVideoFrames)?v:0 );
      if(showDelta) {
	displayStateAsGraphs(coeffsFull, &coeffsFree, eDelta, nullptr);
      }
      if(showFree) {
	displayStateAsGraphs(coeffsFree, ((Coeffs<ModState>*)0), eFree, nullptr);
      }
    }
    if (gen==0 && (showFree || showDelta)) {
      sleep(6);
    }

    if (
	e && ezx_isclosed(e) 
	|| 
	eDelta && ezx_isclosed(eDelta) 
	|| 
	eFree && ezx_isclosed(eFree) 
	) break;

    usleep(10);

    for (int updates=0; updates<updatesBetweenDisplays; ++updates) {
      coeffsFull.update();      
      if (computeFree) {
	coeffsFree.update();
      }

      if (calc1PNormAllTheTime) {
	
	Real norm1PTmp = 0;
	for (int x=0; x<N; ++x) {
	  ModState simple;
	  simple.incrementMode(x);
	  const Comp & z0 = coeffsFull.slow_getCoeff(simple);
	  norm1PTmp += norm(z0);
	}
	
	if (norm1PTmp>norm1P) {
	  norm1P = norm1PTmp;
	}
      }
    }
  }


  ezx_quit(e);
  if (showDelta) {
    ezx_quit(eDelta);
  }
  
  exit(EXIT_SUCCESS);
}
