
#ifndef LESTER_DISPLAYUTILS_H
#define LESTER_DISPLAYUTILS_H

extern "C" {
#include "ezxdisp.h"
}

#include "Types.h"
#include "config.h"
#include "globals.h"

Real granularTheta(const Real theta) {
  // can't have too many colours or X gets upset, so discretize our theta.  As we have RGB, make the discretisation a multiple of 3.  12 seems nice.
  
  const int grains = 6;

  const Real scale = grains/(2.*lesterPi);
  return floor(theta*scale)/scale;
}

const ezx_color_t * myBlack() {
  return &ezx_black;
}

const ezx_color_t * oldPhaseCol(const Comp & z0) {

  static ezx_color_t col;

  
  // 1st guess
  //col.r = (sin(theta)+1.)/2.;
  //col.g = (cos(theta)+1.)/2.;
  //col.b= 0.5;

  // 2nd guess
  Real theta = arg(z0);

  col.r = 0.5*(sin(granularTheta(theta))+1.);
  theta += 2*lesterPi/3;
  col.g = 0.5*(sin(granularTheta(theta))+1.);
  theta += 2*lesterPi/3;
  col.b = 0.5*(sin(granularTheta(theta))+1.);

  return &col;

  //return &ezx_black;
}

Real discretise(const Real lam, int lumps) {
  return floor(lam*lumps)/lumps;
}

const int phaseInt(const Comp & z0) {
  return 128;

}

Real lamVal(const Comp & z0) {
  
  // Non linear scale to reveal interesting things about the norm ... lam=0 is norm0, lam=1 is norm1 // lam is non-linear to show things you want to see.

  // 0<lam<0.5 reserved for norm nbelow thresh

  const Real n = norm(z0);

  if (n<fancyColsThresh) {
    return 0.5*n/fancyColsThresh;
  } else {
    const Real zeroToOne = (n-fancyColsThresh)/(1.-fancyColsThresh);
     return 0.5+ 0.5*pow(zeroToOne,0.2);
  }; 
  

}


const ezx_color_t * phaseCol(const Comp & z0) {

  static ezx_color_t col;

  Real lam = lamVal(z0);

  if (monochrome) {
    lam = discretise(lam,256); // needed to keep the number of X colours small and ezxdisp fast.
    
    col.r = col.g = col.b = lam;
  } else {
    lam = discretise(lam,16); // needed to keep the number of X colours small and ezxdisp fast.

    Real theta = arg(z0);
    
    col.r = 0.5*(sin(granularTheta(theta))+1.);
    theta += 2*lesterPi/3;
    col.g = 0.5*(sin(granularTheta(theta))+1.);
    theta += 2*lesterPi/3;
    col.b = 0.5*(sin(granularTheta(theta))+1.);
    const Real len = fmax(fmax(col.r, col.g),col.b);
    
    // if n is close to 1 I want, in effect, to divide all col vals by len, to make the colour as "bright" as possible.
    // if n is close to 0 I want, in effect, to set all vals to 1, to make the colour as "white" as possible.
    // inbetween I want to do inbetweeny things.

    col.r = (1.-lam)*1. + lam*col.r/len;
    col.g = (1.-lam)*1. + lam*col.g/len;
    col.b = (1.-lam)*1. + lam*col.b/len;
    
  }
  return &col;

}

#endif
