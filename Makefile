.PHONY: default runQuantum runUnitTest clean check

default: runQuantum

# For g++ within macports: (needs export SDKROOT=`xcrun --show-sdk-path` )
CPP = g++-mp-12

# For system commpiler (clang?)
# CPP = g++

runQuantum: quantum
	./quantum

runUnitTest: UnitTest
	./UnitTest

OBJS = Workspace.o States.o Ket.o DerivativeMatrix.o displayStateAsGraphs.o displayStateAsParticles.o Coeffs.o EnergyCache.o displayUtils.o MixedState.o VideoEncoder.o

check: $(OBJS)

EZX_DIR = ezxdisp-0.1.4

#FFTWLIB_DIR = /atlas/lester/usr/local/lib
#FFTWINC_DIR = /atlas/lester/usr/local/include
FFTWLIB_DIR = /opt/local/lib
FFTWINC_DIR = /opt/local/include

quantum: $(wildcard *.h) $(wildcard *.cc) Makefile

UnitTest: $(wildcard *.h) $(wildcard *.cc) Makefile

#VIDEOFLAGS := -DLESTER_VIDEO -lavcodec -lavutil -lm -lz -lpthread -D__STDC_CONSTANT_MACROS
VIDEOFLAGS := 

% : %.cc $(EZX_DIR)/src/x11/libezx.a
	$(CPP) -g -o $@ $<  -I $(EZX_DIR)/include/ -I $(FFTWINC_DIR) -lX11 -L $(EZX_DIR)/src/x11 -l ezx -L $(FFTWLIB_DIR) -lfftw3 -lm $(VIDEOFLAGS)  -O3

%.o : %.cc
	$(CPP) -g -c $<  -I $(EZX_DIR)/include/ -I $(FFTWINC_DIR) $(VIDEOFLAGS)  -O3 

clean:
	rm -f quantum quantum-strange *~ callgrind.out* \#* *.o UnitTest $(EZX_DIR)/src/x11/libezx.a $(EZX_DIR)/src/x11/libezx.o

$(EZX_DIR)/src/x11/libezx.a:
	make -C $(EZX_DIR)/src/x11
