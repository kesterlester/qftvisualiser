
#ifndef LESTER_STATES_H
#define LESTER_STATES_H

#include <iostream>
#include <iterator>
#include <set>
#include <cassert>

class OccState;
class ModState;
class MixedState;
std::ostream & operator<<(std::ostream &, const OccState &);
std::ostream & operator<<(std::ostream &, const ModState &);
std::ostream & operator<<(std::ostream &, const MixedState &);

typedef std::set<int> NonZeroModes;

#include "utils.h"
#include "EnergyCache.h"


#define ENERGYFUNC \
  Real getEnergy() const {\
    static EnergyCache s_cache;\
    Real ans=0;\
    for (int i=0; i<N; ++i) {\
      ans += (getOccupancyOfMode(i))*(s_cache[i]);\
    }\
    return ans;\
  }


class Thingy {
 public:
  virtual int getNumParticles() const = 0;
  virtual void setVacuum() = 0;
  virtual void setFirstStateWithNumParticles(int num) = 0;
  virtual bool isVacuum() const = 0;
  //static virtual Thingy getVacuum() = 0;
  virtual void incrementMode(int mode) = 0;
  virtual void decrementMode(int mode) = 0; // not checked
  virtual int getOccupancyOfMode(int mode) const = 0;
  virtual bool decrementModeAndIndicateSuccess(int mode) = 0; // you can't decrement a mode that's already unexcited and still get a state (you get 0 not a state).  So return true if the decrement succeeded.  But return false (and leave the state unchanged) if the decrement is impossible.
  virtual std::ostream& printTo(std::ostream & os) const = 0;
private:
};

class OccState {
  friend class ModState;
  friend class MixedState;
 public:
  ENERGYFUNC;
  OccState() {
    setVacuum();
  }
  OccState(const ModState &);
  bool isVacuum() const {
    return m_tot==0;
  }
  OccState & operator++() {

#include "OccStatePlusPlusImp.h"

    return *this;
  }
  bool operator!=(const OccState & other) const {
#include "OccStateNEOp.h"
  }
  bool operator==(const OccState & other) const {
    return !(*this != other);
  }
  void setVacuum() {
    std::fill(m_occ, m_occ+N, 0);
    m_tot=0;
  }
  void setFirstStateWithNumParticles(int numParticles) {
    std::fill(m_occ, m_occ+N, 0);
    m_occ[0]=numParticles;
    m_tot=numParticles;
  }
  inline int getNumParticles() const {
    return m_tot;
  }
  void incrementMode(int mode) {
    assert(mode>=0);
    assert(mode<N);
    ++(m_occ[mode]);
    ++m_tot;
  }
  void decrementMode(int mode) {
    assert(mode>=0);
    assert(mode<N);
    --(m_occ[mode]);
    --m_tot;
  }
  bool decrementModeAndIndicateSuccess(int mode) {
    assert(mode>=0);
    assert(mode<N);
    if (m_occ[mode]>0) {
      --(m_occ[mode]);
      --m_tot;
      return true;
    } else {
      return false;
    }
  }
  // FIXME -- next function has poor O(N) complexity -- probably should use ModState only.
  void storeNonZeroModesIn(NonZeroModes & nonZeroModes) const {
    nonZeroModes.clear();
    for (int i=0; i<N; ++i) {
      if (m_occ[i]) {
	nonZeroModes.insert(i);
      }
    }
  }
  // Note, this ordering is supposed to be identical to the ordering used for correpsonding ModStates.
  bool operator<(const OccState & other) const {
    if (m_tot < other.m_tot) {
      return true;
    } else if (m_tot > other.m_tot) {
      return false;
    } else {
      const int * i = m_occ;
      const int * j = other.m_occ;
      for (; i<m_occ+N; (++i,++j)) {
	if (*i > *j) { // counter intuitive direc, yes, but keeps ordering identical to that of corresponding ModState
	  return true;
	} else if (*i<*j) { // counter intuitive direc, yes, but keeps ordering identical to that of corresponding ModState
	  return false;
	}
      }
      return false;
    }
  }
  std::ostream & printTo(std::ostream & os) const {
    os << "|";
    std::copy(m_occ,m_occ+N,std::ostream_iterator<int>(os, ","));
    return os << "occs[np=" << getNumParticles() << "]>";
  }
  int getOccupancyOfMode(int mode) const {
    assert(mode>=0);
    assert(mode<N);
    return m_occ[mode];
  }
 private:
  int m_occ[N];
  int m_tot;
};

std::ostream & operator<<(std::ostream & os, const OccState & occ) {
  return occ.printTo(os);
}

#include <set>

class ModState {
  friend class OccState;
  friend class DerivativeMatrix;
 public:
  ENERGYFUNC;
  ModState() {};
  ModState(const OccState &);
  void setVacuum() {
    m_modes.clear();
  }
  void setFirstStateWithNumParticles(int numParticles) {
    m_modes.clear();
    for (int i=0; i<numParticles; ++i) {
      m_modes.insert(0);
    }
  }
  bool operator!=(const ModState & other) const {
    return m_modes != other.m_modes;
  }
  bool operator==(const ModState & other) const {
    return m_modes == other.m_modes;
  }
  // Note that the following ordering for ModStates is (at time of writing) the same as the ordering for corresponding OccStates.  i.e.
  //
  // OccState os1(some args);
  // OccState os2(some other args);
  // ModState ms1(os1);
  // ModState ms2(os2);
  // (os1<os2) == (ms1<ms2); // WILL BE TRUE, AT PRESENT!
  //
  // Furthermore, the ordering for ModState is chosen to be
  // compatible with the ordering induced by the ++operator. So:
  // ModState ms1;
  // ModState ms2 = ms1;
  // ++ms2;
  // ms1<ms2; // guaranteed to be true.
  bool operator<(const ModState & other) const {
    if (m_modes.size() < other.m_modes.size()) {
      return true;
    } else if (m_modes.size() > other.m_modes.size()) {
      return false;
    } else {
      // use in-built lexicographical comparator:
      return m_modes < other.m_modes;
      /*
      std::multiset<int>::const_iterator i = m_modes.begin();
      std::multiset<int>::const_iterator j = other.m_modes.begin();
      while (i!=m_modes.end()) {
	if (*i < *j) {
	  return true;
	} else if (*i > *j) {
	  return false;
	}
	++i; ++j;
      }
      // everything was the same!
      return false;
      */
    }
  }
  bool isVacuum() const {
    return m_modes.empty();
  }
  int getNumParticles() const {
    return m_modes.size();
  }
  void incrementMode(int mode) {
    assert(mode>=0);
    assert(mode<N);
    m_modes.insert(mode);
  }
  void decrementMode(int mode) {
    assert(mode>=0);
    assert(mode<N);
    m_modes.erase(m_modes.find(mode));
  }
  bool decrementModeAndIndicateSuccess(int mode) {
    assert(mode>=0);
    assert(mode<N);
    const std::multiset<int>::iterator it = m_modes.find(mode);
    if (it!=m_modes.end()) {
      m_modes.erase(it);
      return true;
    } else {
      return false;
    }
  }
  // operator++ moves us to the next mode in the order that is used by the array format.
  ModState & operator++() {
    // starting at RHS, find the place where we can begin the it.
    std::multiset<int>::reverse_iterator it = m_modes.rbegin();
    
    while (true) {
      if (it == m_modes.rend()) {
	// oh dear, we didn't find a place to increment, so we need to increase the number of particles by one, and fill with zeros:
	const int newSize = getNumParticles()+1;
	m_modes.clear();
	for (int i=0; i<newSize; ++i) {
	  m_modes.insert(0);
	}
	// we are done
	break;
      } else {
	const int i = (*it)+1;
	if (i<N) {
	  // We found the spot that we can increment!
	  // We must not forget to set all that are to the right of us, though!
	  const_cast<int&>(*it)=i;
	  while (it!=m_modes.rbegin()) {
	    --it;
	    const_cast<int&>(*it)=i;
	  }
	  
	  break;
	}
      }
      ++it;
    }
    return *this;
  }
  std::ostream & printTo(std::ostream & os) const {
    os << "|";
    std::copy(m_modes.begin(),m_modes.end(),std::ostream_iterator<int>(os, ","));
    return os << "mods[np=" << getNumParticles() << ",ind("<<getIndex()<<")]>";
    return os;
  }
  int getOccupancyOfMode(int mode) const {
    assert(mode>=0);
    assert(mode<N);
    return m_modes.count(mode);
  }

 public:

  unsigned long getIndex() const {
    unsigned long tmp=0;
    int otherThing = 0;
    const int np = getNumParticles();
    //std::cout << "Trying to use\n";
    std::multiset<int>::const_iterator it = m_modes.begin();
    
    for (int i=0; i<np; (++i, ++it)) {
      //std::cout << "bigF^(" << N-otherThing << ")_("<<np<<"-"<<i<<") ["<<(*it)<<"-"<<otherThing<<"]"<< std::endl;
      tmp += bigF(N-otherThing,
		  np-i,
		  (*it)-otherThing);
      otherThing = (*it);
    }
    // Strictly, the answer we should return is:
    //     tmp + ourBinom(N+np-1,N)
    // however our binomial calculator does not return 0 when
    // faced with second arg bigger than first arg, so 
    // we correct for that there (rather than in ourBinom) since
    // we don't want to slow ourBinom down:
    if (np) {
      return tmp + ourBinom(N+np-1,N);
    } else {
      return tmp;
    }
  }

 private:
  std::multiset<int> m_modes;
};

ModState::ModState(const OccState & os) {
  for(int mode=0; mode<N; ++mode) {
    for(int j=0; j<os.m_occ[mode]; ++j) {
      m_modes.insert(mode);
    }
    if (getNumParticles() == os.getNumParticles()) {
      break;
    }
  }
}

OccState::OccState(const ModState & ms) {
  std::fill(m_occ, m_occ+N, 0);
  m_tot = ms.getNumParticles();
  for (std::multiset<int>::const_iterator it = ms.m_modes.begin();
       it != ms.m_modes.end();
       ++it) {
    ++(m_occ[*it]);
  }
}

std::ostream & operator<<(std::ostream & os, const ModState & ms) {
  return ms.printTo(os);
}

#endif
