#!/bin/sh

echo Usage:
echo
echo "  cat DerivMat-config-medium-3-particles.dat | ./plotMat.sh"
echo

gawk '/#row/{ x=$2 } /index/ {y=$6;v=$8; print log(x+1),log(y+1)}' | hist2D n 200 overwrite
