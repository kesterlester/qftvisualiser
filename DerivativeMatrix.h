#ifndef LESTER_DERIVATIVEMATRIX_H
#define LESTER_DERIVATIVEMATRIX_H

#include <vector>
#include <set>
#include <cassert>
#include <iomanip>
#include <algorithm>

#include "MatrixEltFor.h"
#include "EnergyCache.h"
#include "Workspace.h"
#include "utils.h" // for factorial
#include "MixedState.h"

template<class T>
class Coeffs; // fwd decl

class ModState; // fwd decl
class OccState; //fwd decl

#include <set>
//#include <fstream>

//std::ofstream * debugStream;

const bool DB = false;


class DerivativeMatrix {
public:  

  class SignedMode {
  public:
    int mode;
    bool isDagger;
    SignedMode() {}
    SignedMode(int mode, bool isDagger) : mode(mode), isDagger(isDagger) {}	
    static SignedMode getUp(int mode) { return SignedMode(mode, true ); }
    static SignedMode getDn(int mode) { return SignedMode(mode, false); }
    bool operator==(const SignedMode & other) const {
      return mode==other.mode && isDagger==other.isDagger;
    }
    bool operator!=(const SignedMode & other) const {
      return mode!=other.mode || isDagger!=other.isDagger;
    }
    bool operator<(const SignedMode & other) const {
      if (mode<other.mode) {
	return true;
      } else if (mode>other.mode) {
	return false;
      } else {
	return isDagger != other.isDagger;
      }
    }
  };

  typedef std::multiset<SignedMode> FlexibleSignedModes;

  /*
  class SnoopStateThing {
    // Think of this as a 2*N box, which contains mostly zeros, except in some slots, where the COUNT of the number of "necessaryModes" are kept.  As we apply daggers etc, we upgrade/downgrade this SnoopStateThing, keeping track of the number of nuisancePairsRequired etc ... the intention is (of course) that when we exhaust the number of nuisance paris, THEN we would like to have an object from which it is easy (fast) to extract the locations of the non-zero elements (the remaining nuisance pairs).  To this end, it is helpful to maintain an associated list of non-zero elts.
  public:
    unsigned int occ[2*N]; // the 2 is because the first N are daggers, and the second N are non-daggers ... the format is 2*mode+s for the occupancy of (mode,s) .. this is so that the complementary indices are adjacent .. i.e. the order i
    unsigned int numNuisancePairsRequired; 
    // (0+),(0-),(1+),(1-),(2+),(2-) etc ...
    typedef std::set<unsigned int> NonZeroElts;
    NonZeroElts nonZeroElts;
  };
  */

  typedef std::vector<MatrixEltFor> MatrixElts;

  DerivativeMatrix() : m_found(0), m_contribs(0), matrixElts(Workspace::size())  {
  }
  std::ostream & printTo(std::ostream & os) const {
    return os << "DerivativeMatrix[]";
  }
private:
  template<typename PVec>
  static void undoUpdateMom(PVec & p,
			    const int mode, const int i) {
    if (i==1) {
      p.decrementMode(mode);
    } else {
      p.incrementMode(mode);
    }
  }
  template<typename PVec>
  static void updateMom(PVec & p,
			Real & scaleSq,
			const int mode, const int i) {
    // returns true if worth keeping going:
    static EnergyCache cache;
    scaleSq /= (2.*cache[mode]);
    if (i==1) {
      // aDagger
      p.increment(mode);
      scaleSq*=p.getOccupancyOfMode(mode);
    } else {
      const int n=p.getOccupancyOfMode(mode);
      if(p.decrementMode(mode)) {
	scaleSq=0; // don't actually need this line
      } else {
	scaleSq*=n; 
      }
    }
  }
public:
  class MinimalChanges {
  public:
    template<class PVec>
    static bool conservedThingIsBadOrChangesAreTooGreat(const PVec & from, const PVec & to) {
      //std::cout << "Set conserved thing to zero " << std::endl;
      int conservedThing = 0;
      int changes=0;
      for (int mode=0; mode<N; ++mode) {
	const int f = from.getOccupancyOfMode(mode);
	const int t = to.getOccupancyOfMode(mode);
	const int diff = t-f;
	conservedThing += diff*mode;
        changes += abs(diff);
      }
      return changes>PHIPOWER || (conservedThing%N)!=0;
    }
    template<class PVec>
    MinimalChanges(const PVec & from, const PVec & to) {
      //std::cout << "Set conserved thing to zero " << std::endl;
      for (int mode=0; mode<N; ++mode) {
	const int f = from.getOccupancyOfMode(mode);
	const int t = to.getOccupancyOfMode(mode);
	const int diff = t-f;
	if (diff>0) {
	  for (int i=0; i<diff; ++i) {
	    signedModes.insert(SignedMode::getUp(mode));
	  }
	} else if (diff<0) {
	  for (int i=0; i<-diff; ++i) {
	    signedModes.insert(SignedMode::getDn(mode));
	  }
	}
      }
    }
  public:
    FlexibleSignedModes signedModes;
  };

  template<typename PVec>
  void snoop(Real & ampForThisPQ, // can be added to by contributions to the amplitude as they are found
		    const int numberOfNuisancePairsRequired,
		    PVec & theMomSoFar, // can be modified internally (for speed) but must be left unchanged on exit
#ifndef NDEBUG
		    const PVec & destinationMom, // the mom we are trying to get "theMomSoFar" promoted to
                    const PVec startMomQ,
                    const int depth,
#endif
		    FlexibleSignedModes & necessarySignedModes, // this is modifiable for speed, but MUST be left unchanged on return
		    const Real scaleSq//,
		    //   const std::string & debugOps
		    ) {
    
    if (DB) {
      std::cout << "================" << std::endl;
      std::cout << "snoop called with args\namp " << ampForThisPQ << "\nnumNuisPairs " << numberOfNuisancePairsRequired << "\ntheMomSoFar " << theMomSoFar
#ifndef NDEBUG
 <<"\ndestMomP " << destinationMom
 <<"\nstartMomQ " << startMomQ 
 <<"\ndepth " << depth
#endif
 << "\nscaleSq " << scaleSq << " and necessarySignedModes["<<necessarySignedModes.size()<<"]\n";
      std::copy(necessarySignedModes.begin(),necessarySignedModes.end(),std::ostream_iterator<SignedMode>(std::cout, ", "));
      std::cout << "\n................." << std::endl;
    }
	  // We are trying to promote momentum q (slowly being modified in "mom" below) up(down) to momentum p.
	  // To avoid wasting time trying to (say) promote it way past p or way below p, it may be helpful to set out with some idea of
	  // the maximum or minimum number of creation or annihilation operators we are going to need, or which modes we are going to need, so that we don't waste our time with pointless attempts at creation and annihilation.

    static EnergyCache cache;

    assert(scaleSq);
    assert(numberOfNuisancePairsRequired>=0);

    // FIXME -- it would be faster to make the terminal step NOT require a function call!  Could intagrate this at an earlier stage!
    if (numberOfNuisancePairsRequired==0 && necessarySignedModes.size()==0) {
      // OK - no more to apply ... we SHOULD have reached the destination mom
      assert(destinationMom == theMomSoFar);
      ampForThisPQ += sqrt(scaleSq);
      ++m_contribs;
      //(*debugStream) << std::setprecision(11) << "MOO " << scaleSq << " startq[" << startMomQ.getOccupancyOfMode(0) << "," << startMomQ.getOccupancyOfMode(1) << "] tmsf[" << theMomSoFar.getOccupancyOfMode(0) << "," << theMomSoFar.getOccupancyOfMode(1) << "] destp["<<destinationMom.getOccupancyOfMode(0)<<","<<destinationMom.getOccupancyOfMode(1) << "]" << std::endl;
      //std::cout << std::setprecision(11) << "MOO " << scaleSq << " startq[" << startMomQ.getOccupancyOfMode(0) << "," << startMomQ.getOccupancyOfMode(1) << "] tmsf[" << theMomSoFar.getOccupancyOfMode(0) << "," << theMomSoFar.getOccupancyOfMode(1) << "] destp["<<destinationMom.getOccupancyOfMode(0)<<","<<destinationMom.getOccupancyOfMode(1) << "]" << std::endl;
    } else {
      
      // Only get here if there is actually something to do:
      if (numberOfNuisancePairsRequired==0) {
	// the number of nuisancePairs is ==0, so we just want to choose modes from among "necessarySignedModes".  However, necessarySignedModes includes some modes more than once, and we only want to loop over one copy of each, so (1) we are going to have to keep track of which mode types we have applied, and (2) we are going to have to generate a new necessaryModesList that excludes one copy of the mode currently being applied.  That would be BEST done with a multiset that allowed individual modes to be removed  in place.  That's a potential future speed improvement.  For now we just make a new copy of the list.  FIXME

	std::set<SignedMode> uniqueSignedModes;
	for(FlexibleSignedModes::const_iterator it = necessarySignedModes.begin();
	    it != necessarySignedModes.end();
	    ++it) {
	  uniqueSignedModes.insert(*it);
	}

	// so now loop over the unique modes it is worth applying:
	for (std::set<SignedMode>::const_iterator it = uniqueSignedModes.begin();
	     it!=uniqueSignedModes.end();
	     ++it) {

	  // fiddle with the  necessarySignedModes list to remove the part that we are "using" ... but remember it so that we can put it back in afterwards.
	
	  const SignedMode signedModeWeMustPutBackLater = *it;
	  FlexibleSignedModes::const_iterator itForDeletion = necessarySignedModes.find(signedModeWeMustPutBackLater);
	  assert(itForDeletion != necessarySignedModes.end());          

	  // now we actually do the erasure .... we MUST put this back later!
	  necessarySignedModes.erase(itForDeletion);

	  const int & mode = it->mode;
	  const bool thisModeIsDagger = it->isDagger;
	  const int newNumberOfNuisancePairsRequired = 0; 
		    
	  /// START VERY SIMILAR
#include "snippet.h"
	  /// END VERY SIMILAR

	  // here we put back the deleted mode
	  necessarySignedModes.insert(signedModeWeMustPutBackLater);

	}
	
      } else {
	// the number of nuisancePairs is >=0, so loop over all possible modes, both up and down.  However, we must be careful about how we record what still needs to be "done".
	
	// If one of the modes we try to process is in the list of necessarySignedModes, we need to remove it from that list, and leave the number of numberOfNuisancePairs unaltered.
	
	// If, on the other hand, one of the modes we try is NOT in the list of necessarySIgnedModes, then we need to "use up" one of our nuisancePairs, and insert the complementary part of the nuisance pair into the list of necessaries. 


	SignedMode modeBeingApplied; // This is always needed!
	SignedMode newModeForNecessaryList; // This is not always needed.

	for (int s=0; s<2; ++s) {
          newModeForNecessaryList.isDagger = !(modeBeingApplied.isDagger = static_cast<bool>(s)); // note here how we enforce the complementarity.
	  for (modeBeingApplied.mode=0; modeBeingApplied.mode<N; ++(modeBeingApplied.mode)){
            newModeForNecessaryList.mode = modeBeingApplied.mode; // note here how we enforece the complementarity

          // we had better look to see whether the modeBeingApplied is in the
	  // existsing "necessarySignedModes":

	  FlexibleSignedModes::const_iterator it = necessarySignedModes.find(modeBeingApplied);
	  if (it!=necessarySignedModes.end()) {

            // ok, we are applying a mode that IS in the list of necessaries.  This measns that this is not really a nuisance mode.  It is a "necessary mode".  So keep the number of nuisance modes unchanged, and (temporarily) shrink the necessaryModes:

            const bool & thisModeIsDagger = modeBeingApplied.isDagger;
	    const int & mode              = modeBeingApplied.mode;
	    const int newNumberOfNuisancePairsRequired = numberOfNuisancePairsRequired;

            // Take out the mode being applied
	    necessarySignedModes.erase(it);
	    /// START VERY SIMILAR
#include "snippet.h"
	    /// END VERY SIMILAR

	    // put back the mode being applied.
            necessarySignedModes.insert(modeBeingApplied);

          } else {
	      // .. ok .. we are applying a mode that IS NOT in the list of necessaries, so this is a bonafide "up/down" nusiance mode. As a consequence, we must add the (complematary) mode to the necessaryModeSet, (AND DELTE IT AFTERWARDS!) and decrease the number of nuisancepairs by one.

            const bool & thisModeIsDagger = modeBeingApplied.isDagger;
	    const int & mode              = modeBeingApplied.mode;
	    const int newNumberOfNuisancePairsRequired = numberOfNuisancePairsRequired-1;

            // Insert the complementary mode:
	    necessarySignedModes.insert(newModeForNecessaryList);
	    /// START VERY SIMILAR
#include "snippet.h"
	    /// END VERY SIMILAR

	    // Take the complementary mode out again:
	    FlexibleSignedModes::const_iterator toDelete = necessarySignedModes.find(newModeForNecessaryList);
	    assert(toDelete!=necessarySignedModes.end());
            necessarySignedModes.erase(toDelete);
          }

	  } // newMode;
	} // s
	
	
      } // 
    } // there is something to do
  } // end of function
 

         
#include "setupNew.h"


  /////////////////////////////////////////////////////////////////////////////////////


  template<typename PVec>
  void setupOLD() {

    //std::ofstream file;
    //debugStream = &file;
    //file.open("moo1.dat");

    globalFound=0;
    int rows=0;
    std::cout << "Setting up derivative matrix OLD \n";
    const int size = Workspace::size();
    PVec pState;

    for(int pIndex = 0;
	pIndex != size;
	(++pIndex, ++pState)) {
      ++rows;

      MatrixEltFor & matrixEltFor = matrixElts[pIndex];


      /// PUT IN A CONTRIBUTION FROM THE FREE HAMILTONIAN
      {
	MatrixEltFor::Thing thing;
	thing.index = pIndex;
	thing.factor = pState.getEnergy(); // NOTE WE OMIT THE MINUS I PRE-FACTOR
	matrixEltFor.things.push_back(thing);
      }

      /// NOW TRY TO FIND CONTRIBUTIONS FOR THE INTERACTION HAMILTONIAN 
 
      //std::cout << "*" << std::flush;
      const PVec & p(pState);
      const PVec & destinationMom = p;


      if (lambda!=0 && PHIPOWER>0) {


	// at best we can lower or raise number of particles by PHIPOWER, so
	
	const int minNumberOfParticlesWorthConsideringForQ = std::max(p.getNumParticles()-PHIPOWER, 0                 );
	const int maxNumberOfParticlesWorthConsideringForQ = std::min(p.getNumParticles()+PHIPOWER, globalMaxParticles);


	for (int numberOfParticlesInQ = minNumberOfParticlesWorthConsideringForQ; 
	     numberOfParticlesInQ <= maxNumberOfParticlesWorthConsideringForQ; 
	     ++numberOfParticlesInQ) {
	  
	  // NOTE .. an interaction of the form phi^EVEN can only
	  // mix states with even-even or odd-odd multiplicites,
	  // and vice-versa, so:
	  
	  if ((numberOfParticlesInQ + p.getNumParticles() + PHIPOWER )%2 != 0) {
	    // Don't need to carry on.
	    // This is a big speed-up!
	    continue;
	  }

	  const int qStartIndex = Workspace::beginIndexForNumParticles(numberOfParticlesInQ);
	  const int qStopIndex  = Workspace::  endIndexForNumParticles(numberOfParticlesInQ);
	  PVec qState;
	  qState.setFirstStateWithNumParticles(numberOfParticlesInQ); // state corresponding to qStartIndex;

	  for(int qIndex = qStartIndex;
	      qIndex < qStopIndex;
	      (++qIndex, ++qState)) {
	    
	    const PVec & q(qState);
	    
	    if (MinimalChanges::conservedThingIsBadOrChangesAreTooGreat(q, destinationMom)) {
	      // give up .. since the OTHER (non minimal changes) will come in up down pairs, and so will not influence the conservedThing
	      continue;
	    }
	    
	    MinimalChanges minimalChanges(q, destinationMom); // not combined with first test, since first thing fails more frequently than this one.

	    assert(minimalChanges.signedModes.size()<=PHIPOWER); // should be enforeced by conservedThingIsBadOrChangesAreTooGreat test
	    assert((PHIPOWER - minimalChanges.signedModes.size())%2 == 0);
	    
	    // OK .. by this point we know that it WILL be possible to make some valid transitions with this P-Q pair -- such transitions will INCLUDE the "minimalChanges.signedMode" plus some other number of up down (or down up) pairs of "nuisance modes".
	    
	    const int numberOfNuisancePairsRequired = (PHIPOWER - minimalChanges.signedModes.size())/2;
	    if (DB) {
	      std::cout << "MINIMAL CHANGES SIGNED MODES SIZE " << minimalChanges.signedModes.size() << std::endl;
	    }
	    
	    // Make a copy of q that it can be modified ... actually, now that we are confident that theMomSoFar is not being messed up, we could experiment with (1) making the actual qState modifiable, or (2) using a different type (Mod or Occ) for the qState as for the "theMomSoFar" for snoop. FIXME
	    
	    PVec theMomSoFar = q; // FIXME ..
	    
	    Real ampForThisPQ = 0;
	    const Real scaleSq = 1;
	    
	    ////////////////////////////////////
	    // Start doing stuff here!
	    ////////////////////////////////////
	    
#ifndef NDEBUG
	    const PVec debug = theMomSoFar;
#endif
	    //	  const std::string debugOps = ":";
	    
	    if (DB) {
	      std::cout << "========= TOTALLY NEW PQ PAIR =========" << std::endl;
	    }
	    
	    snoop(ampForThisPQ, // can be added to by contributions to the amplitude as they are found
		  numberOfNuisancePairsRequired,
		  theMomSoFar, // can be modified internally (for speed) but must be left unchanged on exit
#ifndef NDEBUG
		  destinationMom, // the mom we are trying to get "theMomSoFar" promoted to
                  q,
                  0, //depth
#endif
		  minimalChanges.signedModes,
		  scaleSq//,
		  //	debugOps
		  );
	    
	    
	    assert(theMomSoFar == debug);
	    
	    ////////////////////////////////////
	    // Have finished doing stuff here!
	    ////////////////////////////////////
	    
	    
	    // By this point, we ought to have a valid finished form of "ampForThisPQ".
	    if (ampForThisPQ) {
	      if (showHintTerms) {
		std::cout << "[<"<<p<<"|Hint|"<<q<<">="<<ampForThisPQ<<"]" << std::endl;
	      } 
	
	      
	      static const Real otherFactor = bigL*lambda/factorial(PHIPOWER)/sqrt(pow(bigL,PHIPOWER));  // this is our interaction coupling constant, times some other factors, see bayesian lab book (59)
	      
	      MatrixEltFor::Thing thing;
	      thing.index = qIndex;
	      thing.factor = ampForThisPQ*otherFactor;  // NOTE WE OMIT THE MINUS I PRE-FACTOR
	      
	      if (globalFound%1000==0) {
		std::cout << "Found " << globalFound << " elts" << " from " << m_contribs << " contributions which is " << (globalFound+0.0)/rows << " elts per row.   " << p.getNumParticles() <<  std::endl;
	      }

	      //matrixEltFor.takeNoteOf(thing);
	      matrixEltFor.things.push_back(thing);
	      ++globalFound;

	    }
	      
	  } //qIndex Loop
	  
	  
	}// number of particles in Q

      } // lambda  !=0 

      //std::cout << "Found-new " << globalFound << " per row " << (globalFound+0.0)/rows << " with " << p.getNumParticles() << std::endl;
      
    } // pIndex loop   
    assert(matrixElts.size()==Workspace::size());
    std::cout << "Found " << globalFound << " elts" << " from " << m_contribs << " contributions, in TOTAL, which is " << (globalFound+0.0)/rows << " elts per row.   "<<  std::endl;



    //file.close();

    m_found = globalFound;

  } // setupMatrixelt func OLD
 
public:

  long int getNumberOfInteractionElements() const {
    return globalFound; // NOT SAFE -- GLOBAL KLUDGE -- MORE THAN ONE DM WILL CLASH - FIXME
  }
  // data
  

  friend class Coeffs<OccState>;
  friend class Coeffs<ModState>;
  
  std::ostream & dumpTo(std::ostream & os) const {
    os << std::setprecision(11) << "Type DerivativeMatrix "
       << "Version 1 "
       << "N " << N << " "
       << "PHIPOWER " << PHIPOWER << " "
       << "globalMaxParticles " << globalMaxParticles << " " 
       << "matrixElts.size " <<  matrixElts.size() << "\n";
    int row=0;
    for (MatrixElts::const_iterator it = matrixElts.begin();
	it!=matrixElts.end();
	(++it,++row)) {
        os << "#row " << row << "\n";
	it->dumpTo(os);
        os << "\n";
    } 
    return os;
  }
  std::istream & readFrom(std::istream & is) {
    YUMS("Type");
    YUMS("DerivativeMatrix");
    YUMS("Version");
    YUMS("1");
    YUMS("N");
    YUMI(N);
    YUMS("PHIPOWER");
    YUMI(PHIPOWER);
    YUMS("globalMaxParticles");
    YUMI(globalMaxParticles);
    YUMS("matrixElts.size");
    long int rows;
    is >> rows;
    assert(rows == Workspace::size());
    matrixElts.resize(rows);
    for (int row=0; row<rows; ++row) {
      YUMS("#row");
      YUMI(row);
      MatrixEltFor & matrixEltFor = matrixElts[row];
      matrixEltFor.readFrom(is);
    } 
    return is;
  }
public:
  bool operator!=(const DerivativeMatrix & other) const {
    return matrixElts != other.matrixElts;
  }
  bool operator==(const DerivativeMatrix & other) const {
    return matrixElts == other.matrixElts;
  }
  Real compareTo(const DerivativeMatrix & other) const {
    if (matrixElts.size() != other.matrixElts.size()) {
      std::cout << "Incomparable at " << __LINE__ << " of " << __FILE__ << std::endl;
      throw "Incomparable";
    }
    Real biggestDiff = 0;
    MatrixElts::const_iterator it1 = matrixElts.begin();
    MatrixElts::const_iterator it2 = other.matrixElts.begin();
    for (;it1!=matrixElts.end(); (++it1, ++it2)) {
      const Real diff = it1->compareTo(*it2);
      if (diff>biggestDiff) {
	biggestDiff = diff;
      }
    }
    return biggestDiff;
  }
  long getContributions() const {
     return m_contribs;
  }
  void sortMe() {
    for (MatrixElts::iterator it = matrixElts.begin();
	 it != matrixElts.end();
	 ++it) {
      it->sortMyThings();
    }
  }
private:
  MatrixElts matrixElts;
  long m_contribs;
  long m_found;
};

std::ostream & operator<<(std::ostream & os, const DerivativeMatrix::SignedMode & sm) {
  return os << "SignedMode[" << sm.mode << (sm.isDagger ? "^" : "v") << "]";
}

#endif
