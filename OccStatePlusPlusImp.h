   // The basic operation of incrementing consists finding the rightmost non-zero.  If this digit is not against the RH margin, then it can be decremented by 1, and that 1 can be transferred (additively if necessary) to the space to its right.  However if that digit is against the RH margin, then it is necessary to (1) remember the RH margin digit, (2) set the RH margin digit to 0 (3)  try find the first non-zero digit to the left of the zero that was previously found, decrement it by one, and set the digit to its right to 1+theNumberJustRemembered.  NB .. the above won't work if the RH digit is equal to numParticles, since there will be no other non-zero digit. In this case, one instead increases numParticles by 1, and places numParticles in the left column, and zeros elsewhere.
  
    if (m_tot==0) {

      // deal with vacuum separately:
      m_occ[0]=1;
      m_tot=1;

    } else {

      // We are not dealing with the vacuum.
      
      // Find rightmost non-zero digit ... we guarantee there is one
      int indexOfRightmostNonZeroDigit = N-1;
      while(m_occ[indexOfRightmostNonZeroDigit]==0) {
	--indexOfRightmostNonZeroDigit;
      }      
      // now we have found the rightmost non-zero digit
      
      if (indexOfRightmostNonZeroDigit == N-1) {

	// Our rightmost non-zero digit was in the margin.

	if (m_occ[indexOfRightmostNonZeroDigit]==m_tot) {
	  // then we'll need to increase the number of paricles, since our state looks like this: 0,0,0,0,0,0,0,numParticles and needs to go to numParticles+1,0,0,0,0,0,0,0

	  m_occ[0] = ++m_tot;
	  if (N-1 != 0) {
	    m_occ[N-1] = 0;
	  }

	} else {

	  // Do the "ripple".

	  // First find the next-non-zero number to the left of our current digit -- we guarantee there is one!
	  int indexOfNextNonZeroDigit = indexOfRightmostNonZeroDigit-1;
	  while (m_occ[indexOfNextNonZeroDigit]==0) {
	    --indexOfNextNonZeroDigit;
	  }
	  
	  // now do the "ripple" itself.
	  const int transfer = m_occ[indexOfRightmostNonZeroDigit];
	  m_occ[indexOfRightmostNonZeroDigit] = 0;   // lost transfer  from m_tot
	  --(m_occ[indexOfNextNonZeroDigit]);        // lost transfer+1 from m_tot
	  m_occ[indexOfNextNonZeroDigit+1] = transfer+1; // m_tot is restored.
	  
	
	}

      } else {
	// We are not against RH margin, so we do the simple decrement transfer:
	--(m_occ[indexOfRightmostNonZeroDigit]);
	m_occ[indexOfRightmostNonZeroDigit+1]=1;
      }
      
    }
