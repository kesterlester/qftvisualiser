
#ifndef CONFIG_H_PROTECTION
#define CONFIG_H_PROTECTION

#define REALMAT
#undef CHECK_NORMS

int width=625, height=600;

//#include "config-all-vacuum.h"   // (0a) -- with and without showFree
//#include "config-minimal.h"
//#include "config-small-space-4-particles.h"

//#include "config-standard.h"
//#include "config-medium.h"
//#include "config-medium-asymmetric-impact.h" // (0c)
#include "config-medium-asymmetric-impact-withPhases.h" // (0b)
//#include "config-huge-colour.h"


//#include "config-lindstrom.h"
//#include "config-lindstrom-7-particles.h"
//#include "config-minimal.h"
//#include "config-standard.h"
//#include "config-UnitTest.h"

//#include "config-medium.h" // (3)
//#include "config-medium-delta.h"
//#include "config-medium-weak-coupling.h"
//#include "config-medium-3-particles.h"   // (4a)
//#include "config-medium-3-particles-all-shown.h"     // (4b)
//#include "config-medium-asymmetric-impact.h"
//#include "config-medium-asymmetric-impact-4-particles.h"

//#include "config-planewave.h"
//#include "config-planewaves-clash.h"
//#include "config-fermigoldenrule-test.h"

//#include "config-arc-fancy-cols.h"
//#include "config-arc-conventional.h"

//#include "config-huge-monochrome.h"
//#include "config-huge-colour.h"

//#include "config-gigantic-monochrome.h"

//#include "config-huge-monochrome-3-particles.h"

#endif

