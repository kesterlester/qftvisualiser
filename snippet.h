{
	    Real nextScaleSq;

            bool doRest = true;

	    // ** mess with momSoFar
	     
	    if (thisModeIsDagger) {
	      theMomSoFar.incrementMode(mode);
	      nextScaleSq = scaleSq*theMomSoFar.getOccupancyOfMode(mode) / (2.*cache[mode]);;
	    } else {
	      assert(!thisModeIsDagger);
	      // aNotDagger
	      const int n=theMomSoFar.getOccupancyOfMode(mode);
	      if (!(theMomSoFar.decrementModeAndIndicateSuccess(mode))) {
		doRest = false; // nb .. don't need to undo changs to theMomSoFar, as there have been no changes!
	      } else {
		nextScaleSq = scaleSq * n / (2.*cache[mode]);; 
	      }
	    }

	    if (doRest) {
	    assert(nextScaleSq);

	    // ** //////// Now do things
	    
	    snoop(ampForThisPQ, // can be added to by contributions to the amplitude as they are found
		  newNumberOfNuisancePairsRequired,
		  theMomSoFar, // can be modified internally (for speed) but must be left unchanged on exit
#ifndef NDEBUG
		  destinationMom, // the mom we are trying to get "theMomSoFar" promoted to
		  startMomQ, // the mom we are trying to get "theMomSoFar" promoted to
                  depth+1,
#endif
		  necessarySignedModes,
		  nextScaleSq//,
		  // debugOps+os.str()
		  );
	    	    
	    // ** ////// Should have finished doing things
      
	    // ** undo mess with momSoFar
	    
	    if (thisModeIsDagger) {
	      // undo aDagger
	      theMomSoFar.decrementMode(mode);
	    } else {
	      assert(!thisModeIsDagger);
	      // undo aNotDagger
	      theMomSoFar.incrementMode(mode);
	    }
	    	    
            }
}
