#ifndef LESTER_VISREP_H
#define LESTER_VISREP_h

#include "config.h"

template<typename T>
class TwoDimData {
 public:
  const T & operator[](const int i) const {
    return data[i];
  }
  T & operator[](const int i) {
    return data[i];
  }
  T data[N*N];
};


#endif
