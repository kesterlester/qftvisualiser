
#ifndef LESTER_MIXEDSTATE_H
#define LESTER_MIXEDSTATE_H

#include <algorithm>
#include <iostream>
#include <iterator>
#include <cassert>
#include "States.h"
#include "utils.h"

class MixedState;
std::ostream & operator<<(std::ostream &, const MixedState &);


class MixedState {
  friend class DerivativeMatrix;

 private:
  OccState m_occState;

  int m_modes[globalMaxParticles+1]; // "+1" since we need to be able to hold a state that is "one past the end". .. this ensures that we can increment the "last" state, and get something that can represent an end() iterator

 public:

  ENERGYFUNC;

  MixedState() : m_occState() {
  }


 public: // should be private but exposed for unit test

 public:
  unsigned long getIndex() const {
    unsigned long tmp=0;
    int otherThing = 0;
    const int np = getNumParticles();
    //std::cout << "Trying to use\n";
    for (int i=0; i<np; ++i) {
      //std::cout << "bigF^(" << N-otherThing << ")_("<<np<<"-"<<i<<") ["<<m_modes[i]<<"-"<<otherThing<<"]"<< std::endl;
      tmp += bigF(N-otherThing,
		  np-i,
		  m_modes[i]-otherThing);
      otherThing = m_modes[i];
    }
    // Strictly, the answer we should return is:
    //     tmp + ourBinom(N+np-1,N)
    // however our binomial calculator does not return 0 when
    // faced with second arg bigger than first arg, so 
    // we correct for that there (rather than in ourBinom) since
    // we don't want to slow ourBinom down:
    if (np) {
      return tmp + ourBinom(N+np-1,N);
    } else {
      return tmp;
    }
  }

  explicit MixedState(const OccState &);

  inline int getNumParticles() const {
    return m_occState.m_tot;
  }

  bool isVacuum() const {
    return m_occState.isVacuum();
  }
 private:
  void imp_opPlusPlus() {
    //std::cout << "BEFORE ++ " << (*this) << std::endl;
    /*

    The aim of this routine is to increment the contents of m_modes through the following sort of iterative cycle (this example assumes N=5):

     {}       np=0,
     {0},     np=1,
     {1},     np=1,
     {2},     np=1,
     {3},     np=1,
     {4},     np=1,
     {0,0},   np=2,
     {0,1},   np=2,
     {0,2},   np=2,
     {0,3},   np=2,
     {0,4},   np=2,
     {1,1},   np=2,
     {1,2},   np=2,
     {1,3},   np=2,
     {1,4},   np=2,
     {2,2},   np=2,
     {2,3},   np=2,
     {2,4},   np=2,
     {3,3},   np=2,
     {3,4},   np=2,
     {4,4},   np=2,
     {0,0,0}, np=3,
     {0,0,1}, np=3,
     {0,0,2}, np=3,
     {0,0,3}, np=3,
     {0,0,4}, np=3,
     {0,1,1}, np=3,
     {0,1,2}, np=3,
     {0,1,3}, np=3,
     {0,1,4}, np=3,
     {0,2,2}, np=3,
     {0,2,3}, np=3,
     {0,2,4}, np=3,
     {0,3,3}, np=3,
     {0,3,4}, np=3,
     {0,4,4}, np=3,
     {1,1,1}, np=3,
     {1,1,2}, np=3,
     {1,1,3}, np=3,
     {1,1,4}, np=3,
     {1,2,2}, np=3,
     {1,2,3}, np=3,
     {1,2,4}, np=3,
*/
    int * const m_modes_rbegin = m_modes+getNumParticles()-1;
    int * const m_modes_rend = m_modes-1;
    int * it = m_modes_rbegin;
    
    // starting at RHS, find the place where we can begin the it.
    while (true) {
      if (it == m_modes_rend) {
	// oh dear, we didn't find a place to increment, so we need to increase the number of particles by one, and fill with zeros:
	const int newSize = getNumParticles()+1;
	//std::cout  << "Oh dear, doing " << newSize << " fills " << std::endl;
	assert(newSize<=globalMaxParticles); // Had "+1" but in 2022 I thinik that's wrong. So I've removed the +1
	for (int * m=m_modes; m<m_modes+newSize; ++m) {
	  (*m)=0;
	}
	// we are done
	break;
      } else {
	const int i = (*it)+1;
        //std::cout << "i "<< i << " " << __FILE__ << " " << __LINE__ << std::endl; 
        //std::cout << "(*it) "<< (*it) << " " << __FILE__ << " " << __LINE__ << std::endl; 
        //std::cout << "it "<< it << " " << __FILE__ << " " << __LINE__ << std::endl; 
        //std::cout << "m_modes_rbegin "<< m_modes_rbegin << " " << __FILE__ << " " << __LINE__ << std::endl; 
	if (i<N) {
          //std::cout << "i<N " << i << " < " << N << " " __FILE__ << " " << __LINE__ << std::endl; 
	  // We found the spot that we can increment!
	  // We must not forget to set all that are to the right of us, though!
          //std::cout << "it/m_modes_rbegin " << it << " " << m_modes_rbegin << " " <<  __FILE__ << " " << __LINE__ << std::endl; 
	  (*it)=i;
          //std::cout << "it/m_modes_rbegin " << it << " " << m_modes_rbegin << " " <<  __FILE__ << " " << __LINE__ << std::endl; 
	  while (it!=m_modes_rbegin) {
            //std::cout << "HUULOO " << __FILE__ << __LINE__ << std::endl;
	    ++it;
            //std::cout << "HOOLOO " << __FILE__ << __LINE__ << std::endl;
            //if (it==m_modes_rbegin) { std::cout << "HOHO it == rbegin " << __FILE__ << __LINE__ << std::endl; } else { std::cout << "HOHO it!= rbegin " << __FILE__ << __LINE__ << std::endl; }
            // This program has (in 2022) been noticed seg-faulting on the next line having entered this loop INCORRECTLY, i.e. when it == m_modes_rbegin. Cause not found. Problem seen with O3 optimisation in clang
	    (*it)=i;
            //std::cout << "HOOLOO " << __FILE__ << __LINE__ << std::endl;
	  }
	  
	  break;
	}
      }
      --it;
    }
  }
 public:
  MixedState & operator++() {
    assert(getNumParticles()<=globalMaxParticles);
    imp_opPlusPlus(); // must come before "++m_occState" since it uses getNumParticles()
    ++m_occState;
    return *this;
  }

  bool operator!=(const MixedState & other) const {
    if (getNumParticles() != other.getNumParticles()) {
      // they have different number of particles
      return true;
    }
    // both have the same number of particles
    for (int i=0; i<getNumParticles(); ++i) {
      if (m_modes[i] != other.m_modes[i]) {
	// they are different
	return true;
      }
    }
    // we couldn't find a difference, so they are identical
    return false;
  }
  bool operator==(const MixedState & other) const {
    return !(*this != other);
  }
  bool deepEqualityTest(const MixedState & other) const {
    if (m_occState != other.m_occState) {
      return false;
    }
    for (int i=0; i<getNumParticles(); ++i) {
      if (m_modes[i]!=other.m_modes[i]) {
	return false;
      }
    }
    return true;  
  }
  void setVacuum() {
    m_occState.setVacuum();
  }
 private:
  void imp_setFirstStateWithNumParticles(int numParticles) {
    assert(numParticles<=globalMaxParticles+1);
    for (int i=0; i<numParticles; ++i) {
      m_modes[i]=0;
    }
  }
 public:
  void setFirstStateWithNumParticles(int numParticles) {
    m_occState.setFirstStateWithNumParticles(numParticles);
    imp_setFirstStateWithNumParticles(numParticles);
  }

 private:
  void imp_incrementMode(int mode) {
    int * insertPos = std::lower_bound(m_modes, m_modes+getNumParticles(), mode);
    // loop over remaining parts of vector, pushing them one space further on:
    for (int * newPos = m_modes+getNumParticles(); 
	 newPos > insertPos;
	 --newPos) {
      *newPos = *(newPos-1);
    }
    *insertPos = mode;
  }
  void imp_decrementMode(int mode) {
    // this method is not checked .. so we MUST be able to delete it, and it MUST exist:
    assert(getNumParticles()>=1);

    int * erasePos = std::lower_bound(m_modes, m_modes+getNumParticles(), mode);

    // this method is not checked .. so we MUST be able to delete it, and it MUST exist:
    assert(erasePos != m_modes+getNumParticles());
    assert(*erasePos == mode);

    // loop over remaining parts of vector, pushing them one space further left
    for (int * pos = erasePos; 
	 pos < m_modes+getNumParticles()-1; // guaranteed >=0
	 ++pos) {
      *pos = *(pos+1);
    }
  }
 public:
   void incrementMode(int mode) {
    assert(mode>=0);
    assert(mode<N);
    
    // imp must come fist, as it uses getNumParticles();
    imp_incrementMode(mode);

    // this can come second:
    m_occState.incrementMode(mode);
  }
  void decrementMode(int mode) {
    assert(mode>=0);
    assert(mode<N);
 
   // imp must come fist, as it uses getNumParticles();
    imp_decrementMode(mode);

    // this can come second:
    m_occState.decrementMode(mode);
  }
 public:
  bool decrementModeAndIndicateSuccess(int mode) {
    assert(mode>=0);
    assert(mode<N);
    if (m_occState.m_occ[mode]>0) {
      decrementMode(mode);
      return true;
    } else {
      return false;
    }
  }
  // FIXME -- next function has poor O(N) complexity -- probably should use ModState only.
  void storeNonZeroModesIn(NonZeroModes & nonZeroModes) const {
    nonZeroModes.clear();
    int lastMode = -1;
    for (int i=0; i<getNumParticles(); ++i) {
      const int mode = m_modes[i];
      if (mode != lastMode) {
	lastMode = mode;
	nonZeroModes.insert(i);
      }
    }
  }
  std::ostream & printTo(std::ostream & os) const {
    //std::cout <<"\n";
    const int IND = getIndex();
os << "|MIX:";
    std::copy(m_occState.m_occ,m_occState.m_occ+N,std::ostream_iterator<int>(os, ","));
    os << "&:";
    std::copy(m_modes,m_modes+getNumParticles(),std::ostream_iterator<int>(os, ","));
    return os << "mixed,[np=" << getNumParticles() << ",ind("<< IND <<")]>";
  }
  int getOccupancyOfMode(int mode) const {
    assert(mode>=0);
    assert(mode<N);
    return m_occState.m_occ[mode];
  }
  // Note, this ordering is supposed to be identical to the ordering used for correpsonding ModStates and OccStates:
  bool operator<(const MixedState & other) const {
    if (getNumParticles() < other.getNumParticles()) {
      return true;
    } else if (getNumParticles() > other.getNumParticles()) {
      return false;
    } else {
      // use in-built lexicographical comparator:
      for (int i=0; i<getNumParticles(); ++i) {
	if (m_modes[i] < other.m_modes[i]) {
	  return true;
	} else if (m_modes[i] > other.m_modes[i]) {
	  return false;
	}
      }
      // everything was equal, so
      return false;
    }
  }
 private:
};

std::ostream & operator<<(std::ostream & os, const MixedState & occ) {
  return occ.printTo(os);
}



MixedState::MixedState(const OccState & os) : m_occState(os) {
  int * m = m_modes;
  assert(os.getNumParticles()<=globalMaxParticles+1);
  for(int mode=0; mode<N; ++mode) {
    for(int j=0; j<os.m_occ[mode]; ++j) {
      assert(m - m_modes <= globalMaxParticles);
      (*m) = mode;
      ++m;
    }
  }
  assert(m == m_modes + os.getNumParticles()); 
}

/*
MixedState::MixedState(const ModState & ms) {
  std::fill(m_occ, m_occ+N, 0);
  m_tot = ms.getNumParticles();
  for (std::multiset<int>::const_iterator it = ms.m_modes.begin();
       it != ms.m_modes.end();
       ++it) {
    ++(m_occ[*it]);
  }
}
*/

#endif
