#ifndef LESTER_UTILS_QCD_H
#define LESTER_UTILS_QCD_H

#include <cstdlib>
#include "Types.h"

#define YUMS( x  ) { std::string tmp ; is >> tmp ; if (false) {std::cout << "YUMS Expect["<<x<<"] Got["<<tmp<<"] at line " << __LINE__ << " of " << __FILE__ << std::endl;} ; if( tmp != x ) {std::cout << "YUMS PROBLEM WITH Expect["<<x<<"] Got["<<tmp<<"] at line " << __LINE__ << " of " << __FILE__ << std::endl; exit(__LINE__);}} 
#define YUMI( x  ) { int tmp ; is >> tmp ;  if (false) {std::cout << "YUMS Expect["<<x<<"] Got["<<tmp<<"] at line " << __LINE__ << " of " << __FILE__ << std::endl;}; if( tmp != x) {std::cout << "YUMI PROBLEM WITH |Expect["<<x<<"] Got["<<tmp<<"] at line " << __LINE__ << " of " << __FILE__  << std::endl; exit(__LINE__);}} 

Real mod(Real x, int n) {
  return (int)(x+1.*n/2) %  n  -       1.*n/2;
}

int factorial(int n) {
  if (n==0) {
    return 1;
  };
  return n*factorial(n-1);
}

float lrand() {
  return static_cast<float>(rand())/RAND_MAX;
}

  static double crudeBinom(unsigned int n, unsigned int k) {
    if (k>n) return 0;
    // make k small if not already so:
    if (k>n-k) k=n-k;
    double ans=1;
    for (int i=0; i<k; ++i) {
      ans *= static_cast<double>(n-i)/(i+1);
    }
    return ans;
  }
  

  static unsigned long long ourBinom(unsigned long long n, unsigned long long k) {
    assert(n>=k); // this implementation should not be called with n<k, even though such binomial coefficients exist, as it is not designed to handle them.  You were warned!

    //std::cout << "BINOM[" << n << ","<< k<<"]=";
    unsigned long long r = 1, d = n-k;
    /* choose the smaller of k and n - k */
    if (d > k) { k = d; d = n - k; }

    // By this time, d should be the smaller and k the larger part.
    assert(d<=k);

    while (n > k) {
      // DON't BOTHER CHECKING FOR OVERFLOW: 
      // if (r >= UINT_MAX / n) return 0; /* overflown */
      r *= n--;
      
      /* divide (n - k)! as soon as we can to delay overflows */
      while (d > 1 && !(r % d)) r /= d--;
    }
    assert(d==1 || d==0);
    //std::cout << r << std::endl;

    return r;
  }

  static unsigned long bigF(const int n, const int j, const int A) {
    const int tmp = n+j-1;
    return ourBinom(tmp,j) - ourBinom(tmp-A,j);
  }

static const Comp minusI(0,-1);

#endif
