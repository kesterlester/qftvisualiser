#ifndef LESTER_DISPLAYSTATEASPARTICLES_H
#define LESTER_DISPLAYSTATEASPARTICLES_H

extern "C" {
#include "ezxdisp.h"
}

#include "Coeffs.h"

void displayStateAsParticles(const Coeffs & coeffs, ezx_t *e) {
  

  const Real normNow = coeffs.normNow();
  const Real plottingNorm = static_cast<Real>(rand())/static_cast<Real>(RAND_MAX) * normNow;
  Real normSoFar = 0;

  {
    static const Real desiredArea = N*lumpSize*lumpSize;

    const int width = (int)sqrt(desiredArea*OVERALL_SCALE*coeffs.m_absError);
    ezx_fillrect_2d(e, 
		    1, 
		    1,
		    1+width,
		    1+width,
		    &ezx_black);   
      

      
  }

  int xOff = lumpSize;
  int yOff = lumpSize;
  const int halfWid = static_cast<int>(lumpSize/4.);

  for (int i=0; i<=maxParticleStatesToShow && i<=globalMaxParticles; ++i) {
    int yPos = yOff+i*lumpSize;
    ezx_fillrect_2d(e, xOff, yPos, xOff+N*lumpSize, yPos+1, &ezx_black);	  
  }

  if (maxParticleStatesToShow>=0 && globalMaxParticles>=0) {
    const Comp & z0 = coeffs.slow_getCoeff(PVec::zero());
    normSoFar += norm(z0);
    if (normSoFar>=plottingNorm) {
      int yPos = yOff+0*lumpSize;
      ezx_fillrect_2d(e, xOff, yPos, xOff+N*lumpSize, yPos+1, &ezx_red);  
      return;
    }
  }
  
  if (maxParticleStatesToShow>=1 && globalMaxParticles>=1) {
    
    static fftw_complex * const in  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    static fftw_complex * const out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    static fftw_plan plan = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    
    for (int x=0; x<N; ++x) {
      const Comp & coeff = coeffs.slow_getCoeff(PVec::zero()+PVec::e(x));
      in[x][0] = coeff.real();
      in[x][1] = coeff.imag();
    }	
    
    fftw_execute(plan);
        
    for (int x=0; x<N; ++x) {
      const Comp  z0=Comp(out[x][0],out[x][1])/sqrt(static_cast<Real>(N));
      normSoFar += norm(z0);
      if (normSoFar>=plottingNorm) {
	int yPos = yOff+1*lumpSize;
	ezx_fillrect_2d(e, xOff, yPos, xOff+N*lumpSize, yPos+1, &ezx_red);  
	ezx_fillrect_2d(e,
			xOff+x*lumpSize-halfWid, yPos-halfWid,
			xOff+x*lumpSize+halfWid, yPos+halfWid,
			&ezx_black);  
	return;
      } // hit jackpot
    
    } // x positions
    
  } // if show 1 particle states
  
  if (maxParticleStatesToShow>=2 && globalMaxParticles>=2) {

    static fftw_complex * const in  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N*N);
    static fftw_complex * const out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N*N);
    static fftw_plan plan = fftw_plan_dft_2d(N, N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    
    for (int x=0; x<N; ++x) {
      for (int y=0; y<N; ++y) {
	const Comp & coeff = coeffs.slow_getCoeff(PVec::zero()+PVec::e(x)+PVec::e(y));
	in[x*N+y][0] = coeff.real();
	in[x*N+y][1] = coeff.imag();
      }
    }
    
    fftw_execute(plan);
    
    for (int x=0; x<N; ++x) {
      for (int y=x; y<N; ++y) {
	const Comp  z0=Comp(out[x*N+y][0],out[x*N+y][1])/sqrt(static_cast<Real>(N))/sqrt(static_cast<Real>(N));

	normSoFar += norm(z0); // This might be wrong as I haven't checked that the FT of the redundant information (in this manner) correctly preserves norm.
	
	if (normSoFar>=plottingNorm) {
	  int yPos = yOff+2*lumpSize;
	  ezx_fillrect_2d(e, xOff, yPos, xOff+N*lumpSize, yPos+1, &ezx_red);  
	  ezx_fillrect_2d(e,
			  xOff+x*lumpSize-halfWid, yOff+2*lumpSize-halfWid,
			  xOff+x*lumpSize+halfWid, yOff+2*lumpSize+halfWid,
			  &ezx_black);  
	  ezx_fillrect_2d(e,
			  xOff+y*lumpSize-halfWid, yOff+2*lumpSize-halfWid,
			  xOff+y*lumpSize+halfWid, yOff+2*lumpSize+halfWid,
			  &ezx_black);  
	  return;
	} // jackpot
	
      } // y vals
    } // x vals

  } // show 2 particles


  if (maxParticleStatesToShow>=3 && globalMaxParticles>=3) {

    static fftw_complex * const in  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N*N*N);
    static fftw_complex * const out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N*N*N);
    static fftw_plan plan = fftw_plan_dft_3d(N, N, N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    
    for (int x=0; x<N; ++x) {
      for (int y=0; y<N; ++y) {
	for (int z=0; z<N; ++z) {
	  const Comp & coeff = coeffs.slow_getCoeff(PVec::zero()+PVec::e(x)+PVec::e(y)+PVec::e(z));
	  const int index = x*N*N+y*N+z;
	  in[index][0] = coeff.real();
	  in[index][1] = coeff.imag();
	}
      }
    }
    
    fftw_execute(plan);
    
    for (int x=0; x<N; ++x) {
      for (int y=x; y<N; ++y) {
	for (int z=y; z<N; ++z) {
	  
	  const int index = x*N*N+y*N+z;
	  
	  const Comp  z0=Comp(out[index][0],out[index][1])/sqrt(static_cast<Real>(N))/sqrt(static_cast<Real>(N))/sqrt(static_cast<Real>(N));
	  
	  normSoFar += norm(z0); // This might be wrong as I haven't checked that the FT of the redundant information (in this manner) correctly preserves norm.
	  
	  if (normSoFar>=plottingNorm) {
	    ezx_fillrect_2d(e, xOff, yOff+3*lumpSize, xOff+N*lumpSize, yOff+3*lumpSize+1, &ezx_red);  
	    ezx_fillrect_2d(e,
			    xOff+x*lumpSize-halfWid, yOff+3*lumpSize-halfWid,
			    xOff+x*lumpSize+halfWid, yOff+3*lumpSize+halfWid,
			    &ezx_black);  
	    ezx_fillrect_2d(e,
			    xOff+y*lumpSize-halfWid, yOff+3*lumpSize-halfWid,
			    xOff+y*lumpSize+halfWid, yOff+3*lumpSize+halfWid,
			    &ezx_black);  
	    ezx_fillrect_2d(e,
			    xOff+z*lumpSize-halfWid, yOff+3*lumpSize-halfWid,
			    xOff+z*lumpSize+halfWid, yOff+3*lumpSize+halfWid,
			    &ezx_black);  
	    return;
	  } // jackpot
	  
	} // z vals
      } // y vals
    } // x vals
    
  } // show 3 particles
  
      



/*




    xOff += N*lumpSize+margin;
  }

  // SHOW THREE PARTICLE STATES
  if (maxParticleStatesToShow>=3 && globalMaxParticles>=3) {
    const int smallLump = static_cast<int>(lumpSize/N);
    for (int x=0; x<N; ++x) {
      for (int y=x; y<N; ++y) {
	for (int z=y; z<N; ++z) {
	  const Comp & z0 = coeffs.slow_getCoeff(PVec::zero()+PVec::e(x)+PVec::e(y)+PVec::e(z));
	  const Real theta = arg(z0);
	  ezx_color_t myColour = {(sin(theta)+1.)/2.,(cos(theta)+1.)/2,0.5};


	  const Real area = desiredArea*sf3*norm(z0);
	  const Real halfWid = 0.5*sqrt(area);

	  const Real xProj[] = {0.2*lumpSize, lumpSize, 0};
	  const Real yProj[] = {0.1*lumpSize, 0, lumpSize};

	  const Real xx = (x*xProj[0] + y*xProj[1] + z*xProj[2]);
	  const Real yy = (x*yProj[0] + y*yProj[1] + z*yProj[2]);

	  ezx_fillrect_2d(e,
			  static_cast<int>(xOff + xx - halfWid),
			  static_cast<int>(yOff + yy - halfWid),
			  static_cast<int>(xOff + xx + halfWid),
			  static_cast<int>(yOff + yy + halfWid),
			  phaseCol(theta));
	  //std::cout << real(z0) << std::endl;
	}
      }
    }
    xOff += (N+1)*(lumpSize+smallLump);
  }
    
*/


}

#endif
