#!/bin/sh

cat DerivMat-config-medium-3-particles.dat  | gawk '/#row/{ x=$2 } /index/ {y=$6;v=$8; print log(x+1),log(y+1)}' | hist2D n 200 lx 4.1 ux 7.5 ly 7.5 uy 11 
