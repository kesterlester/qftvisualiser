#include <cassert>
#include <complex>
typedef double Real;
typedef std::complex<Real> Comp;
#include "config-UnitTest.h"
//#include "config-standard.h"
#include "Workspace.h"
#include "DerivativeMatrix.h"
#include "Coeffs.h"
#include "MixedState.h"
#include "VideoEncoder.h"

#include <iostream>

#include <fstream>

#define CHECK( x , l ) { \
    if (!(x)) {		 \
  ++errors; \
  std::cout << "Error: UnitTest detects something broken at line " << l << " of " << __FILE__ << std::endl; \
  abort(); \
 } \
}

int errors=0;

void test2_5() {
  Workspace & ws = Workspace::getInstance();
  std::cout << ws << "\n";
  CHECK (N==5, __LINE__ ); // could remove ... but I like it
  if (N==5) {
    CHECK ( ws.beginIndexForNumParticles(0)==0 &&
	    ws.endIndexForNumParticles(0)==1, __LINE__ );
    CHECK ( ws.beginIndexForNumParticles(1)==1 &&
	    ws.endIndexForNumParticles(1)==6, __LINE__ );
    CHECK ( ws.beginIndexForNumParticles(2)==6 &&
	    ws.endIndexForNumParticles(2)==21, __LINE__ );
    CHECK ( ws.beginIndexForNumParticles(3)==21 &&
	    ws.endIndexForNumParticles(3)==56, __LINE__ );
    CHECK (ws.size()==56, __LINE__ );

  }
}

template<typename Mod, typename Occ>
void test3(const std::string & message) {
  std::cout << "Testing " << message << " incrementor" << std::endl;
  Mod ms;

  for (int i=0; i<=56; ++i) {
    std::cout << i << "\t" << ms << "\t\t" << Occ(ms) << "\t";

    if (i==0) {
      Mod tmp;
      tmp.setFirstStateWithNumParticles(0);
      CHECK( ms == tmp , __LINE__ );
    } else if (i==1) {
      Mod tmp;
      tmp.setFirstStateWithNumParticles(1);
      CHECK( ms == tmp  , __LINE__ );
    } else if (i==6) {
      Mod tmp;
      tmp.setFirstStateWithNumParticles(2);
      CHECK( ms == tmp  , __LINE__ );
    } else if (i==21) {
      Mod tmp;
      tmp.setFirstStateWithNumParticles(3);
      CHECK( ms == tmp  , __LINE__ );
    }


    Mod prev=ms;
    ++ms;
    const bool newIsBigger1 = (prev<ms);
    const bool newIsBigger2 = !(ms<prev);
    CHECK (newIsBigger1, __LINE__);
    CHECK (newIsBigger2, __LINE__);
    CHECK(ms==Occ(ms), __LINE__);
    // Ordering for OccStates should match for ModStates:
    Occ prevOs(prev);
    Occ newOs(ms);
    const bool newIsBigger3 = (prevOs<newOs);
    const bool newIsBigger4 = !(newOs<prevOs);
    CHECK (newIsBigger3, __LINE__);
    CHECK (newIsBigger4, __LINE__);
    std::cout << (newIsBigger1 && newIsBigger2 && newIsBigger3 && newIsBigger4 ? "order ok ": "order bad ") << std::endl;

    
  }
}


void test3b(const std::string & message) {
  std::cout << "Testing " << message << " incrementor" << std::endl;
  MixedState ms;
  OccState os;
  ModState mods;
  for (int i=0; i<56; ++i) {
    std::cout << i << "\t" << ms << "\t" << os << "\t" << mods << std::endl;

    CHECK( ms == MixedState(os) , __LINE__ );


    if (i==0) {
      MixedState tmp;
      tmp.setFirstStateWithNumParticles(0);
      CHECK( ms == tmp , __LINE__ );
    } else if (i==1) {
      MixedState tmp;
      tmp.setFirstStateWithNumParticles(1);
      CHECK( ms == tmp  , __LINE__ );
    } else if (i==6) {
      MixedState tmp;
      tmp.setFirstStateWithNumParticles(2);
      CHECK( ms == tmp  , __LINE__ );
    } else if (i==21) {
      MixedState tmp;
      tmp.setFirstStateWithNumParticles(3);
      CHECK( ms == tmp  , __LINE__ );
    }


    MixedState prev=ms;
    ++ms; ++os; ++mods;
    const bool newIsBigger1 = (prev<ms);
    const bool newIsBigger2 = !(ms<prev);
    CHECK (newIsBigger1, __LINE__);
    CHECK (newIsBigger2, __LINE__);
    /*
    CHECK(ms==Occ(ms), __LINE__);
    // Ordering for OccStates should match for ModStates:
    Occ prevOs(prev);
    Occ newOs(ms);
    const bool newIsBigger3 = (prevOs<newOs);
    const bool newIsBigger4 = !(newOs<prevOs);
    CHECK (newIsBigger3, __LINE__);
    CHECK (newIsBigger4, __LINE__);
    */

    // std::cout << (newIsBigger1 && newIsBigger2 && newIsBigger3 && newIsBigger4 ? "order ok ": "order bad ") << std::endl;

    
  }
}



void test6() {
  std::cout << "Testing both incrementors do same thing ... " << std::endl;
  OccState os;
  ModState ms;
  MixedState mix;
  bool stillShowMix = true;
  for (int i=0 ;i<=100000; ++i) {
    if (i%100000==0) {
      std::cout << i << std::endl;
    }
    CHECK(os==OccState(ms), __LINE__);
    CHECK(ms==ModState(os), __LINE__);
    if (stillShowMix) {
      CHECK(mix==MixedState(os), __LINE__);
      std::cout << "See ["<<i<<","<<mix.getIndex()<<"]" << std::endl;
      CHECK(i == mix.getIndex(), __LINE__);
    }
    ++os;
    std::cout << "OS++ " << os << std::endl;
    ++ms;
    std::cout << "MS++ " << ms << std::endl;
    if (stillShowMix && mix.getNumParticles()<=globalMaxParticles) {
      ++mix;
      std::cout << "MIX++ " << mix << std::endl;
    } else {
      stillShowMix = false;
    }
  }
  std::cout << " ... done" << std::endl;
}


void test0() {
  MixedState mix;
  OccState os;
  std::cout << "MONKEY   " << os << " " << mix << std::endl;
  CHECK(mix == MixedState(os), __LINE__);
  for (int mode=0; mode<std::min(globalMaxParticles,N); ++mode) {
    mix.incrementMode(mode);
    os.incrementMode(mode);
    std::cout << "MONKEY ^"<<mode<<" " << os << " " << mix << std::endl;
    CHECK(mix == MixedState(os), __LINE__);
  }
  for (int mode=0; mode<std::min(globalMaxParticles,N); ++mode) {
    mix.decrementMode(mode);
    os.decrementMode(mode);
    std::cout << "MONKEY v"<<mode<<" " << os << " " << mix << std::endl;
    CHECK(mix == MixedState(os), __LINE__);
  }
  for (int mode=0; mode<N; ++mode) {
    for (int ups=0; ups<globalMaxParticles; ++ups) {
      mix.incrementMode(mode);
      os.incrementMode(mode);
      std::cout << "MONKEY ^"<<mode<<" " << os << " " << mix << std::endl;
      CHECK(mix == MixedState(os), __LINE__);
    }
    for (int downs=0; downs<globalMaxParticles; ++downs) {
      mix.decrementMode(mode);
      os.decrementMode(mode);
      std::cout << "MONKEY v"<<mode<<" " << os << " " << mix << std::endl;
      CHECK(mix == MixedState(os), __LINE__);
    }
  }
  for (int mode=0; mode<N; ++mode) {
    for (int ups=0; ups<globalMaxParticles-globalMaxParticles/2; ++ups) {
      mix.incrementMode(mode);
      os.incrementMode(mode);
      std::cout << "MONKEY ^"<<mode<<" " << os << " " << mix << std::endl;
      CHECK(mix == MixedState(os), __LINE__);
    }
    for (int ups=0; ups<globalMaxParticles/2; ++ups) {
      mix.incrementMode((mode+1)%N);
      os.incrementMode((mode+1)%N);
      std::cout << "MONKEY ^"<<mode<<" " << os << " " << mix << std::endl;
      CHECK(mix == MixedState(os), __LINE__);
    }
    for (int downs=0; downs<globalMaxParticles-globalMaxParticles/2; ++downs) {
      mix.decrementMode(mode);
      os.decrementMode(mode);
      std::cout << "MONKEY v"<<mode<<" " << os << " " << mix << std::endl;
      CHECK(mix == MixedState(os), __LINE__);
    }
    for (int downs=0; downs<globalMaxParticles/2; ++downs) {
      mix.decrementMode((mode+1)%N);
      os.decrementMode((mode+1)%N);
      std::cout << "MONKEY v"<<mode<<" " << os << " " << mix << std::endl;
      CHECK(mix == MixedState(os), __LINE__);
    }
  }
 
}

void test1()   {
    ModState ms;
    OccState os;
    MixedState mix;
    
    std::cout << ms << "\t\t\t" << os << std::endl;
    

    const int modeToIncr = (N>=3 ? 2 : N-1);
   

    ms.incrementMode(modeToIncr);
    ms.incrementMode(modeToIncr);
    ms.incrementMode(0);
    
    mix.incrementMode(modeToIncr);
    mix.incrementMode(modeToIncr);
    mix.incrementMode(0);
    
    os.incrementMode(modeToIncr);
    os.incrementMode(modeToIncr);
    os.incrementMode(0);
    
    std::cout << ms << "\t\t\t" << os << std::endl;
    

    // check os m_tot is consistent:
    {
      
    }
    
    ModState msFromOs(os);
    OccState osFromMs(ms);
    MixedState mixFromOs(os);

    std::cout << "coverted versions" << std::endl;
    std::cout << msFromOs << "\t\t\t" << osFromMs << std::endl;
    
    //// TEST WE CREATED THINGS THE SAME
    
    CHECK(msFromOs==ms, __LINE__);
    CHECK(osFromMs==os, __LINE__);
    CHECK(mixFromOs==mix, __LINE__);
    
    //// TEST WE CAN COMPARE STATES OF DIFFERENT TYPES USING IMPLICIT CONVERSION
    
    CHECK(osFromMs==ms, __LINE__);
    CHECK(msFromOs==os, __LINE__);
    
    // check inequality ops

    CHECK(!(ms<ms), __LINE__ );
    CHECK(!(os<os), __LINE__ );
    
  }


void test2()   {

   
    ModState ms;
    OccState os;
    
    std::cout << ms << "\t\t\t" << os << std::endl;

    if (N>=4) {
      
      ms.incrementMode(2);  // 2
      ms.incrementMode(2);  // 2,2
      ms.incrementMode(2);  // 2,2,2
      ms.incrementMode(1);  // 1,2,2,2
      ms.decrementMode(2);  // 1,2,2
      ms.incrementMode(3);  // 1,2,2,3

      CHECK( ms.getOccupancyOfMode(0)==0 , __LINE__ );
      CHECK( ms.getOccupancyOfMode(1)==1 , __LINE__ );
      CHECK( ms.getOccupancyOfMode(2)==2 , __LINE__ );
      CHECK( ms.getOccupancyOfMode(3)==1 , __LINE__ );
      
      os.incrementMode(3); // 3
      os.incrementMode(0); // 0,3
      os.incrementMode(2); // 0,2,3
      os.incrementMode(1); // 0,1,2,3
      os.incrementMode(2); // 0,1,2,2,3
      os.decrementMode(0); // 1,2,2,3

      CHECK( os.getOccupancyOfMode(0)==0 , __LINE__ );
      CHECK( os.getOccupancyOfMode(1)==1 , __LINE__ );
      CHECK( os.getOccupancyOfMode(2)==2 , __LINE__ );
      CHECK( os.getOccupancyOfMode(3)==1 , __LINE__ );
      
    } else {

      ms.incrementMode(0); 
      ms.incrementMode(N-1);  
      ms.incrementMode(N-1);  
      ms.incrementMode(0);
      ms.decrementMode(0);
      ms.incrementMode(0);
      ms.decrementMode(N-1);  

      os.incrementMode(0); 
      os.incrementMode(N-1);  
      os.incrementMode(N-1);  
      os.incrementMode(0);
      os.decrementMode(0);
      os.incrementMode(0);
      os.decrementMode(N-1);  


    
    }


    std::cout << ms << "\t\t\t" << os << std::endl;
    
    
     //// TEST WE CREATED THINGS THE SAME
    
    if (os != ms) {
      ++errors;
      std::cout << "Error at " << __FILE__ << ":" << __LINE__ << std::endl;
    }
   
  }



void test7() {
  for (int st=0; st<=8; ++st) {
    const Ket<ModState> initialKet(st);
    std::cout << "Initial Ket for start type " << st << " is " << initialKet << std::endl;
  }
}

void test11() {
	for (int n=0; n<500; ++n) {
	for (int k=0; k<5; ++k) {
        if(n>=k) {
        
	const double b1=crudeBinom(n,k);
	const unsigned long long b2=ourBinom(n,k);
	const double f =  floor(b1-b2+0.5);
        if (f!=0) {
        std::cout << "Expect bin[" << n <<","<<k<<"] crude:" << b1 << " == fancy:" << b2 << std::endl;
        }
	CHECK( f==0 , __LINE__);
  }
}
}
}

void test8() {
	DerivativeMatrix dmOSOld;
	DerivativeMatrix dmMSOld;  
	DerivativeMatrix dmOSNew;
	DerivativeMatrix dmMSNew;  
	dmOSOld.setupOLD<OccState>();
	std::cout << "Found " << dmOSOld.getNumberOfInteractionElements() << " contribs " << dmOSOld.getContributions() << std::endl;
	dmMSOld.setupOLD<ModState>();
	std::cout << "Found " << dmMSOld.getNumberOfInteractionElements() << " contribs " << dmMSOld.getContributions() << std::endl;
	dmOSNew.setup<OccState>();
	std::cout << "Found " << dmOSNew.getNumberOfInteractionElements() << " contribs " << dmOSNew.getContributions() << std::endl;
	//dmMSNew.setupNEW<ModState>();
	//std::cout << "Found " << dmMSNew.getNumberOfInteractionElements() << " contribs " << dmMSNew.getContributions() << std::endl;
        CHECK( dmOSOld.getNumberOfInteractionElements()==176, __LINE__);
        CHECK( dmMSOld.getNumberOfInteractionElements()==176, __LINE__);
        CHECK( dmOSNew.getNumberOfInteractionElements()==176, __LINE__);
        //CHECK( dmMSNew.getNumberOfInteractionElements()==176, __LINE__);
        CHECK (1370 == dmOSOld.getContributions(), __LINE__ );
        CHECK (1370 == dmMSOld.getContributions(), __LINE__ );
        CHECK (1370 == dmOSNew.getContributions(), __LINE__ );
        //CHECK (1370 == dmMSNew.getContributions(), __LINE__ );
   


	// write a file

	std::ofstream file1;
	file1.open("dmOld.dat");
	dmOSOld.dumpTo(file1);
	file1.close();

	std::ofstream file2;
	file2.open("dmNew.dat");
	dmOSNew.dumpTo(file2);
	file2.close();




	CHECK ( dmOSOld==dmMSOld, __LINE__ );
	//CHECK ( dmOSNew==dmMSNew, __LINE__ );
	//CHECK ( dmOSNew.isCloseTo(dmOSOld,1./10000000000), __LINE__ );
}
 
void test9() {

  std::cout << "Preparing initial state ... " << std::endl;
  Ket<ModState> initialKet(startType);
  std::cout << " ... initial state prepared." << std::endl;


  std::cout << "Preparing another derivative matrix ... " << std::endl;
  DerivativeMatrix derivativeMatrix2;
  derivativeMatrix2.setupOLD<OccState>();
  std::cout << " ... derivative matrix prepared." << std::endl;
  
  std::cout << "Preparing derivative matrix ... " << std::endl;
  DerivativeMatrix derivativeMatrix;
  derivativeMatrix.setupOLD<ModState>();
  std::cout << " ... derivative matrix prepared." << std::endl;

 {
   // write a file
   const int siz = derivativeMatrix .getNumberOfInteractionElements();
   std::ostringstream o1;
   o1 << "DerivMat_"<<siz<<"a.dat";
   const std::string name1 = o1.str();
   std::ofstream file1;
   file1.open(name1.c_str());
   derivativeMatrix.dumpTo(file1);
   file1.close();
   
   // read in that file
   DerivativeMatrix dmReadTest;
   std::ifstream file2;
   file2.open(name1.c_str());
   dmReadTest.readFrom(file2);
   file2.close();
   
   // write that file a second time to allow easy comparison
   std::ostringstream o3;
   o3 << "DerivMat_"<<siz<<"b.dat";
   const std::string name3 = o3.str();
   std::ofstream file3;
   file3.open(name3.c_str());
   dmReadTest.dumpTo(file3);
   file3.close();
   
   std::cout << "WORST NORM DIFF IS " << dmReadTest.compareTo(derivativeMatrix) << std::endl;

   // see if the result is the same:
   //CHECK( dmReadTest == derivativeMatrix, __LINE__ ); // ACTUALLY, THIS IS WAY MORE STRINGENT THAN IS REASONABLE... COULD EASILY RELAX ... THINGS NEED NOT GO TO ASCII AND COME BACK WITH BIT PERFECT PRECISION. FEEL FREE TO RELAX AND BASE ON THE WORST NORM DIFF LATER IF THIS BECOMES A PROBLEM
   
 }

  CHECK( derivativeMatrix .getNumberOfInteractionElements()==176, __LINE__);
  CHECK( derivativeMatrix2.getNumberOfInteractionElements()==176, __LINE__);

  std::cout << "Preparing movement states ... " << std::endl;
  Coeffs<ModState> coeffs(initialKet, derivativeMatrix);
  std::cout << " ... movement states prepared." << std::endl;


}

#include <cstdio> // for remove:
#include <cstdlib> // for system()
void testVid() {
        /* 
        remove("/tmp/testVid.mpg");
	VideoEncoder v("/tmp/testVid.mpg");

	v.start();

	v.testEncodeSomeFrames2();

	v.stop();
        
	system("mplayer /tmp/testVid.mpg");
        */
}

int main() {

  // assert(false);
  test11();
  test0();

  test1();
  test2();

  test2_5();
  
  test3<ModState, OccState>("ModState");
  
  test3<OccState, ModState>("OccState");
  

  test3b("MixedState");
  
  test6();

  test7();

  test9();

  test8();

   testVid();

  std::cout << "\nFound " << errors << " errors." << std::endl;
  return errors;
}
