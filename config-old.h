#ifndef LESTER_CONFIG_SPEC
#define LESTER_CONFIG_SPEC

/* Nice tartan

static const int N=12; // the number of sites
const Real bigL = 1;
const Real mass = 1;
int width=700, height=700;
int lumpSize=static_cast<int>(270./N);
const int globalMaxParticles=3;
#define OOOOOOOOOOOOOOOOOOOO_PHIPOWER 3
const int startType = 2; // 0=random, 1=all uniform, 2=just vacuum, 3=just 0 mom mode, 4=2&3, 5=posn packet
const Real desiredNorm = 10;
const Real sf0 =1;// 1;
const Real sf1 =2;//20;
const Real sf2 =1000;//400;
const Real sf3 =1;//400;
const Real POSsf0 =1;// 1;
const Real POSsf1 =100;//20;
const Real POSsf2 =1000;//400;
const Real POSsf3 =1;//400;
const bool showHintTerms = false;
const Real dtQFT = 0.01;
const Real tPrintInterval = 1;
const Real lambda = 0.2;
const int maxParticleStatesToShow=2;
const bool showOnlyUnique=false;
const bool showPositionSpace=true;

*/

/*
  Amazing non-growing:

  static const int N=8; // the number of sites
  const Real bigL = 1;
  const Real mass = 1;
  int width=700, height=500;
  int lumpSize=static_cast<int>(270./N);
  const int globalMaxParticles=4;
  #define OOOOOOOOOOOOOOOOOOOOOOOOO_PHIPOWER 3
  const int startType = 2; // 0=random, 1=all uniform, 2=just vacuum, 3=just 0 mom mode, 4=2 with 3, 5=posn packet
  const Real desiredNorm = 10;
  const Real sf0 =1;// 1;
  const Real sf1 = 10;//20;
  const Real sf2 = 1000;//400;
  const Real sf3 = 10000;//400;
  const Real POSsf0 =1;// 1;
  const Real POSsf1 =1;//20;
  const Real POSsf2 =1;//400;
  const Real POSsf3 =1;//400;
  const bool showHintTerms = false;
  const Real dtQFT = 0.01;
  const Real tPrintInterval = 1;
  const Real lambda = 0.2;
  const int maxParticleStatesToShow=3;
  const bool showOnlyUnique=false;
*/

#endif
