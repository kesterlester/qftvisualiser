#ifndef LESTER_CONFIG_SPEC
#define LESTER_CONFIG_SPEC

// look for norm1P 0.14901 or biggestMidNorm 0.0368435 .... has 274 found int elements

static const int N=16; // // 16 for coll, 26 for wobble  // the number of sites
const Real bigL = 1;
const Real epsilon = bigL/N;
const Real mass = N/bigL; // silly, but needed for backwards compatibility
// int width=700, height=600;
int lumpSize=static_cast<int>(280./N);
const int globalMaxParticles=2;
#define PHIPOWER 3
const int startType = 7; // 0=random, 1=all uniform, 2=just vacuum, 3=just 0 mom mode, 4=2&3, 5=posn packet, 6=moving, 7 = colliding 2d blobs, 8 just mid mom state
const Real deltaBoost1D = 1;
const Real deltaBoost2D = 1;
const Real OVERALL_SCALE=1;
const Real sf0    = OVERALL_SCALE*10;
const Real sf1    = OVERALL_SCALE*10;
const Real sf2    = OVERALL_SCALE*1;
const Real sf3    = OVERALL_SCALE*1;
const Real POSsf0 = OVERALL_SCALE*10;
const Real POSsf1 = OVERALL_SCALE*10;
const Real POSsf2 = OVERALL_SCALE*1;
const Real POSsf3 = OVERALL_SCALE*1;
const bool showHintTerms = false;
const Real dtQFT = 0.01*1.2 / 100       *bigL/N;    // 100 for coll, 20 for wobble//      *bigL/N; // looks silly, but there for backwards compatibility after energy unit bug found -- 
const Real tPrintInterval = 1          *bigL/N; // looks silly, but there for backwards compatibility after energy unit bug found -- 
const Real lambda = 864; //0.2/pow(epsilon,3)*1.05     ;// lam=8000 for N=140, dt=0.01*1.2/5, or  lam =864 for N=16, lam=2000 for N=26 ; lam=4000 for N=40 //819; //0.2/pow(epsilon,3+0*(0.5*PHIPOWER+1));   // (cute if power is 3) // looks silly, but there for backwards compatibility after energy unit bug found -- 
const int maxParticleStatesToShow=2;
const bool showOnlyUnique=false;
const bool showPositionSpace=true;
const bool linearRep=false;
const bool useNewEvolCalc = true;
const bool truncateModes = true;   // can set this to true or false -- you choose -- it might make the setup quicker, but the physics should be the same regardless.  
const int updatesBetweenDisplays = 1000;
const bool useFancyCols = false;
const Real fancyColsThresh = 0.001;
const bool monochrome = false;
const bool calc1PNormAllTheTime = true; // warning ... this slows things down
	    const double BLOBsigma=1.*N/12; // in momentum space
	    const double BLOBmeanPX=+1.*N/4;
	    const double BLOBmeanPY=-1.*N/4;
	    const double BLOBxPos = 1.*N-1.*N/4;
            const double BLOByPos = 1.*N/4;
const char * nameOfDerivativeMatrixDumpFile = "DerivMat-config-standard.dat";
#endif
