
// We used to do this by looping over all potential destination states ... but this was slow because, eg in 3->3 particle transitions, the allowed ones are very rare, and so the speed of the duration of the search was scaling as the number of states^2.  This is bad.

// So instead, we now try a method designed to scale as the number of states*(pow(2,PHIPOWER)

private:

#define APPLY_SIMPLE_UP_OPERATOR					\
  theMomSoFarOcc.incrementMode(mode1);					\
  scaleSq1 = scaleSq0*theMomSoFarOcc.getOccupancyOfMode(mode1)/ (2.*cache[mode1]);

#define APPLY_FULL_UP_OPERATOR						\
  theMomSoFarOcc.incrementMode(mode1);					\
  scaleSq1 = scaleSq0*theMomSoFarOcc.getOccupancyOfMode(mode1)/ (2.*cache[mode1]); 


#define UNAPPLY_UP_OPERATOR					  \
  assert(theMomSoFarOcc.getOccupancyOfMode(mode1)>0);		  \
  theMomSoFarOcc.decrementMode(mode1);				  

/// FIXME ... might be able to make the above faster by caching the results of the earlier tests


#define APPLY_SIMPLE_DOWN_OPERATOR					\
  assert(theMomSoFarOcc.getOccupancyOfMode(mode1)>0);			\
  scaleSq1 = scaleSq0*theMomSoFarOcc.getOccupancyOfMode(mode1)/ (2.*cache[mode1]); \
  theMomSoFarOcc.decrementMode(mode1);					

#define APPLY_FULL_DOWN_OPERATOR					\
  assert(theMomSoFarOcc.getOccupancyOfMode(mode1)>0);			\
  scaleSq1 = scaleSq0*theMomSoFarOcc.getOccupancyOfMode(mode1)/ (2.*cache[mode1]); \
  theMomSoFarOcc.decrementMode(mode1);					

#define UNAPPLY_DOWN_OPERATOR					\
  theMomSoFarOcc.incrementMode(mode1);				


#define DEBUG_SAVE \
  ; //const MixedState copy = theMomSoFarOcc;

#define DEBUG_CHECK \
  ; //assert( copy.deepEqualityTest(theMomSoFarOcc) );

void snoopy(MixedState & theMomSoFarOcc,
	    const int numberOfOperatorsToApply,
	    const Real scaleSq0,
	    const int cs0,
	    MatrixEltFor & matrixEltFor) {

  /*
  std::ostringstream o;
  for (int i=0; i<(PHIPOWER-numberOfOperatorsToApply); ++i) {
    o << "   ";
   }
  std::cout << o.str() << "MOO "<<theMomSoFarOcc << " n " << numberOfOperatorsToApply << " ss " << scaleSq0 << " cs " << cs0 << std::endl;
  */

    Real scaleSq1;

  // For signs:
  // 1 is    dagger and "pos" sign in conservedThing,
  // 0 is notDagger and "neg" sign in conservedThing.
  
  static EnergyCache cache;

  if (numberOfOperatorsToApply==1) {
    
    const int penultimateConservedThing = cs0%N; // must be in range [0,N-1] for later bits to work:
    //   const int remainingBitOfConservedThing = N-penultimateConservedThing; // will be in range [1,N]
    
    // that means that we need to have EITHER:
    //     "down"  negSign (notDagger) and mode1=penultimateConservedThing ..... s1=0
    // OR:
    //     "up"  posSign (   dagger) and mode1= remainingBitOfConservedThing%N  ... s1=1
    

    // consider "down" first:
    {
      const int & mode1 = penultimateConservedThing;
#ifndef NDEBUG
      const int finalConservedThing=cs0 + N-mode1;
      assert(finalConservedThing%N==0);
#endif
      if (theMomSoFarOcc.getOccupancyOfMode(mode1)!=0) {
	// it is worth doing the "down"

	DEBUG_SAVE;

	APPLY_SIMPLE_DOWN_OPERATOR; // since don't need to mess with nonZeroElts 
	
	assert(theMomSoFarOcc.getNumParticles() <= globalMaxParticles);
	assert(theMomSoFarOcc.getNumParticles() >= 0);
	

	MatrixEltFor::Thing thing;
	thing.index = Workspace::mixStateToIndex(theMomSoFarOcc);
	assert(thing.index>=0);
	thing.factor = sqrt(scaleSq1); // NOTE WE OMIT THE MINUS I PRE-FACTOR
	++m_contribs;
	matrixEltFor.takeNoteOf(thing);
	
	UNAPPLY_DOWN_OPERATOR;

	DEBUG_CHECK;

      } // worth trying down
    } // consider down scope
	

    // Now consider "up"
    //std::cout << "Before if theMomSoFarOcc.getNumParticles() " << theMomSoFarOcc.getNumParticles() << " glmaxp " << globalMaxParticles << std::endl; 
    if (theMomSoFarOcc.getNumParticles()<=globalMaxParticles-1) {
      // it is worth applying the "up":
      
      const int  & mode1 /*remainingBitOfConservedThing*/ = (N-penultimateConservedThing)%N; // will be in range [0,N-1]
      
#ifndef NDEBUG
      const int finalConservedThing=cs0 + mode1;
      assert(finalConservedThing%N==0);
#endif

      DEBUG_SAVE;

      APPLY_SIMPLE_UP_OPERATOR; // since don't need to mess with nonZeroElts 
      
      // std::cout << "After if theMomSoFarOcc.getNumParticles() " << theMomSoFarOcc.getNumParticles() << " glmaxp " << globalMaxParticles << std::endl; 
      assert((theMomSoFarOcc.getNumParticles() <= globalMaxParticles));
      assert(theMomSoFarOcc.getNumParticles() >= 0);
      
  
      MatrixEltFor::Thing thing;
      thing.index = Workspace::mixStateToIndex(theMomSoFarOcc);
      assert(thing.index>=0);
      thing.factor = sqrt(scaleSq1); // NOTE WE OMIT THE MINUS I PRE-FACTOR
      ++m_contribs;
      matrixEltFor.takeNoteOf(thing);
      
      UNAPPLY_UP_OPERATOR;

      DEBUG_CHECK;

    } // worth applying "up"
    
  } else {
     
    // a caller prerequisite -- otherwise the call was "pointless".
    assert(theMomSoFarOcc.getNumParticles() <= globalMaxParticles+numberOfOperatorsToApply);
    assert(theMomSoFarOcc.getNumParticles() >= 0);

    // If current number of particles is EQUAL TO globalMaxParticles+numberOfOperatorsToApply, then it is ONLY worth us attepting to lower the current state with NON-dagger operators.

    // If current number if particles is ONE LESS than that, then it is still not worth us raising, since the raise will increase the number of particles, while decreasing the number of operators to apply by one, leading to a net defecit of two. So:



    const bool upIsAllowed = (theMomSoFarOcc.getNumParticles()+1 < globalMaxParticles + numberOfOperatorsToApply) ;
    
    // Here we try the daggers (if allowed)
    if (upIsAllowed) {
      for (int mode1=0; mode1<N; ++mode1) {

	DEBUG_SAVE;

	APPLY_FULL_UP_OPERATOR;	    
	assert(theMomSoFarOcc.getNumParticles() <= globalMaxParticles+numberOfOperatorsToApply-1);
	assert(theMomSoFarOcc.getNumParticles() >= 0);
	
	snoopy(theMomSoFarOcc,
	       numberOfOperatorsToApply-1,
	       scaleSq1,
	       cs0 + mode1,
	       matrixEltFor);
	  
	UNAPPLY_UP_OPERATOR;
	
	DEBUG_CHECK;

      } // mode1
    }
    
    // Here we try the down (non) daggers.
    // Only worth decrementing the non-zero slots:
    int lastMode = -1;
    //std::cout << "Search " << theMomSoFarMod << " " << theMomSoFarOcc << std::endl;
    
    for (int k=0;  k<theMomSoFarOcc.getNumParticles(); ++k) {
      const int mode1 = theMomSoFarOcc.m_modes[k]; // this MUST be a copy, not a reference!
      //std::cout << "FOUND " << mode1 << std::endl;
      if (mode1 != lastMode) {
	lastMode = mode1;
	//std::cout << "STARTED WITH " << theMomSoFarOcc << std::endl;
	
	DEBUG_SAVE;

	APPLY_FULL_DOWN_OPERATOR;
	//std::cout << "IT BECOME    " << theMomSoFarOcc << std::endl;
	
	assert(theMomSoFarOcc.getNumParticles() <= globalMaxParticles+numberOfOperatorsToApply-1);
	assert(theMomSoFarOcc.getNumParticles() >= 0);
	
	snoopy(theMomSoFarOcc,
	       numberOfOperatorsToApply-1,
	       scaleSq1,
	       cs0 + N-mode1,
	       matrixEltFor);
	
	//std::cout << "THAT WAS     " << theMomSoFarOcc << std::endl;

	UNAPPLY_DOWN_OPERATOR; // something bad here
	//std::cout << "ENDING AT    " << theMomSoFarOcc << std::endl;


	DEBUG_CHECK;
      }
    }

  } // the last switch
}

public:
template<typename PVec>
void setup(const bool justFreePart=false) {

  EnergyCache cache;
  NonZeroModes nonZeroModes;

  //std::ofstream file;
  //debugStream = &file;
  //file.open("moo1.dat");

  globalFound=0;
  int rows=0;
  std::cout << "Setting up derivative matrix NEW \n";
  const int size = Workspace::size();
  MixedState pStateOcc;
  for(int pIndex = 0;
      pIndex != size;
      (++pIndex, ++pStateOcc)) {
    ++rows;
    
    //    const int np = pStateOcc.getNumParticles();
    //const int beg = Workspace::beginIndexForNumParticles(np);
    //const int wid = Workspace::endIndexForNumParticles(np)-beg;
    ///const int step = std::max(static_cast<int>(1), static_cast<int>(wid/100));
    if (pIndex%1000==0) {
      std::cout << "Found " << globalFound << " pIndex " << pIndex << " of " << size << ",  NP " << pStateOcc.getNumParticles() << std::endl; //  << " (" << static_cast<int>((pIndex-beg)*100./wid) <<  "% )" << std::endl;
    }

    MatrixEltFor & matrixEltFor = matrixElts[pIndex];


    /// PUT IN A CONTRIBUTION FROM THE FREE HAMILTONIAN
    {
      MatrixEltFor::Thing thing;
      thing.index = pIndex;
      thing.factor = pStateOcc.getEnergy(); // NOTE WE OMIT THE MINUS I PRE-FACTOR
      //matrixEltFor.takeNoteOf(thing);
      matrixEltFor.things.push_back(thing);
    }

    /// NOW TRY TO FIND CONTRIBUTIONS FOR THE INTERACTION HAMILTONIAN 

    if (lambda!=0 && PHIPOWER>0 && !justFreePart) {


      //OccState & theMomSoFarOcc = pStateOcc; // could ACTUALLY work with pState directly, without making copy (faster, since we never need pState in what remains) but it will be easier to debug if we have access to an unaltered theMomSoFar ... so to begin with.  future possibility for speedup, though.  FIXME
      //ModState theMomSoFarOcc = pStateMod; // could ACTUALLY work with pState directly, without making copy (faster, since we never need pState in what remains) but it will be easier to debug if we have access to an unaltered theMomSoFar ... so to begin with.  future possibility for speedup, though.  FIXME

      const Real otherFactor = bigL*lambda/factorial(PHIPOWER)/sqrt(pow(bigL,PHIPOWER));  // this is our interaction coupling constant, times some other factors, see bayesian lab book (59)

      const Real scaleSq0 = otherFactor*otherFactor;	     
      
      snoopy(pStateOcc, PHIPOWER, scaleSq0, 0, matrixEltFor);

    } // if there are interactions

  } // loop over pIndeces    
	

  m_found = globalFound;
  std::cout << "Found " << m_found << " in total, from " << m_contribs << " contribs." << std::endl;

} // setupMatrixelt func NEW
 




