#!/bin/sh
rm -f moo{1,2}.dat sorted{1,2}.dat
make
make -C ../QCDV*pre*
sort moo1.dat > sorted1.dat
sort moo2.dat > sorted2.dat
diff --side-by-side sorted*.dat | less -S
