
// We used to do this by looping over all potential destination states ... but this was slow because, eg in 3->3 particle transitions, the allowed ones are very rare, and so the speed of the duration of the search was scaling as the number of states^2.  This is bad.

// So instead, we now try a method designed to scale as the number of states*(pow(2,PHIPOWER)

private:

template<typename PVec> void snoopy(PVec & theMomSoFar,
				    int numberOfOpsStillToGo,
				    const Real scaleSq0) {
  
  Real scaleSq1;
  for (int s1=0; s1<2; ++s1) {
    for (int mode1=0; mode1<N; ++mode1) {
      if (s1) {
	theMomSoFar.incrementMode(mode1);
	scaleSq1 = scaleSq0*theMomSoFar.getOccupancyOfMode(mode1)/ (2.*cache[mode1]);
      } else {
	scaleSq1 = scaleSq0*theMomSoFar.getOccupancyOfMode(mode1)/ (2.*cache[mode1]);
	theMomSoFar.decrementMode(mode1);
      }
      //	  std::cout << "MOO " << __LINE__ << std::endl;
      if (theMomSoFar.getOccupancyOfMode(mode1)>=0 && 
	  theMomSoFar.getNumParticles() <= globalMaxParticles+2  ) {
	const int cs1=cs0 + (s1?mode1:N-mode1);
	
	/////////////////////////////////////
	
	snoopy(theMomSoFar, numberOfOpsStillToGo-1, scaleSq1);
	
	
	//////////////////////////////////////
	
	
      } // momSoFar is sensible
      if (s1) {
	theMomSoFar.decrementMode(mode1);
      } else {
	theMomSoFar.incrementMode(mode1);
      }
    } // mode1
  } // s1
  
  
}

public:
template<typename PVec>
void setupNEW() {

  EnergyCache cache;

  //std::ofstream file;
  //debugStream = &file;
  //file.open("moo1.dat");

  globalFound=0;
  int rows=0;
  std::cout << "Setting up derivative matrix NEW \n";
  const int size = Workspace::size();
  PVec pState;
  for(int pIndex = 0;
      pIndex != size;
      (++pIndex, ++pState)) {
    ++rows;
    
    //    const int np = pState.getNumParticles();
    //const int beg = Workspace::beginIndexForNumParticles(np);
    //const int wid = Workspace::endIndexForNumParticles(np)-beg;
    ///const int step = std::max(static_cast<int>(1), static_cast<int>(wid/100));
    if (pIndex%1000==0) {
      std::cout << "Found " << globalFound << " pIndex " << pIndex << " of " << size << ",  NP " << pState.getNumParticles() << std::endl; //  << " (" << static_cast<int>((pIndex-beg)*100./wid) <<  "% )" << std::endl;
    }

    MatrixEltFor & matrixEltFor = matrixElts[pIndex];


    /// PUT IN A CONTRIBUTION FROM THE FREE HAMILTONIAN
    {
      MatrixEltFor::Thing thing;
      thing.index = pIndex;
      thing.factor = pState.getEnergy(); // NOTE WE OMIT THE MINUS I PRE-FACTOR
      //matrixEltFor.takeNoteOf(thing);
      matrixEltFor.things.push_back(thing);
    }

    /// NOW TRY TO FIND CONTRIBUTIONS FOR THE INTERACTION HAMILTONIAN 

    if (lambda!=0 && PHIPOWER>0) {


      PVec theMomSoFar = pState; // could ACTUALLY work with pState directly, without making copy (faster, since we never need pState in what remains) but it will be easier to debug if we have access to an unaltered theMomSoFar ... so to begin with.  future possibility for speedup, though.

      // For signs:
      // 1 is    dagger and "pos" sign in conservedThing,
      // 0 is notDagger and "neg" sign in conservedThing.

      const int cs0=0;

      const Real otherFactor = bigL*lambda/factorial(PHIPOWER)/sqrt(pow(bigL,PHIPOWER));  // this is our interaction coupling constant, times some other factors, see bayesian lab book (59)

      const Real scaleSq0 = otherFactor*otherFactor;	     

      //////// level 1 /////////

      for (int s1=0; s1<2; ++s1) {
	for (int mode1=0; mode1<N; ++mode1) {
	  Real scaleSq1;
	  if (s1) {
	    theMomSoFar.incrementMode(mode1);
	    scaleSq1 = scaleSq0*theMomSoFar.getOccupancyOfMode(mode1)/ (2.*cache[mode1]);
	  } else {
	    scaleSq1 = scaleSq0*theMomSoFar.getOccupancyOfMode(mode1)/ (2.*cache[mode1]);
	    theMomSoFar.decrementMode(mode1);
	  }
	  //	  std::cout << "MOO " << __LINE__ << std::endl;
	  if (theMomSoFar.getOccupancyOfMode(mode1)>=0 && 
	      theMomSoFar.getNumParticles() <= globalMaxParticles+2  ) {
	    const int cs1=cs0 + (s1?mode1:N-mode1);
      
	    ///////// up to level 2 /////////


	    for (int s2=0; s2<2; ++s2) {
	      for (int mode2=0; mode2<N; ++mode2) {
		Real scaleSq2;
		//		  std::cout << "MOO " << __LINE__ << std::endl;
		if (s2) {
		  theMomSoFar.incrementMode(mode2);
		  scaleSq2 = scaleSq1*theMomSoFar.getOccupancyOfMode(mode2)/ (2.*cache[mode2]);
		} else {
		  scaleSq2 = scaleSq1*theMomSoFar.getOccupancyOfMode(mode2)/ (2.*cache[mode2]);
		  theMomSoFar.decrementMode(mode2);
		}
		//  std::cout << "MOO " << __LINE__ << std::endl;
		if (theMomSoFar.getOccupancyOfMode(mode2)>=0 && 
		    theMomSoFar.getNumParticles() <= globalMaxParticles+1  ) {
		  const int cs2=cs1 + (s2?mode2:N-mode2);
		  
		  /// up to level 3

#define SLOOW

#ifdef SLOW		  
		  for (int s3=0; s3<2; ++s3) {
		    for (int mode3=0; mode3<N; ++mode3) {
#else
		      ////////////////
  
  // Given that the conserved thing for all PHIPOWER modes must hit 0 (mod N) we now know that there are exactly two possibilities for valid s3 and mode3 ... lets work it out:
  
  
  const int penultimateConservedThing = cs2%N; // must be in range [0,N-1] for later bits to work:
  
  
  // so the final signs and modes must contribute this:

  const int remainingBitOfConservedThing = N-penultimateConservedThing; // will be in range [1,N]

  // that means that we need to have EITHER:
  //       negSign (notDagger) and mode=penultimateConservedThing
  // OR:
  //       posSign (   dagger) and mode= remainingBitOfConservedThing%N
  

  for (int option=0; option<2; ++option) {

    int s3;
    int mode3;
    if (option) {
      s3=0;
      mode3=penultimateConservedThing;
    } else {
      s3=1;
      mode3=remainingBitOfConservedThing%N;
    }

#ifndef NDEBUG
const int cs3=cs2 + (s3?mode3:N-mode3);
assert(cs3%N==0);
#endif
    


    {




		      ////////////////
#endif




		      Real scaleSq3;
		      // std::cout << "MOO " << __LINE__ << std::endl;
		      if (s3) {
			theMomSoFar.incrementMode(mode3);
			scaleSq3 = scaleSq2*theMomSoFar.getOccupancyOfMode(mode3)/ (2.*cache[mode3]);
		      } else {
			scaleSq3 = scaleSq2*theMomSoFar.getOccupancyOfMode(mode3)/ (2.*cache[mode3]);
			theMomSoFar.decrementMode(mode3);
		      }
		      // std::cout << "MOO " << __LINE__ << " " << theMomSoFar << std::endl;

		      // WE GET HERE .... BUG NO FURTHER


		      if (theMomSoFar.getOccupancyOfMode(mode3)>=0 && 
			  theMomSoFar.getNumParticles() <= globalMaxParticles+0  ) {
			const int cs3=cs2 + (s3?mode3:N-mode3);
			
			//std::cout << "MOO " << __LINE__ << std::endl;
			/// up to final level
			
			const int finalConservedThing=cs3;
			//std::cout << "MOO " << __LINE__ << std::endl;
			if (finalConservedThing%N==0) {
			  
			  // This is a valid transition.  Put it in!

			  const Workspace::ModStateMap & theMap = Workspace::modStateToIndexMap();
			  const Workspace::ModStateMap::const_iterator it = theMap.find(theMomSoFar);
			  assert(it!=theMap.end());
			    
			  MatrixEltFor::Thing thing;
			  thing.index = it->second;

			  //std::cout << "Found contrib to transit from " << pState << " to " << theMomSoFar << " index " << thing.index << std::endl;


			  thing.factor = sqrt(scaleSq3); // NOTE WE OMIT THE MINUS I PRE-FACTOR
			  ++m_contribs;
			  matrixEltFor.takeNoteOf(thing);

			  
			  //std::cout << "MOO " << __LINE__ << std::endl;
			}
			
			/// end of final level
			
		      } // momSoFar is sensible
		      if (s3) {
			theMomSoFar.decrementMode(mode3);
		      } else {
			theMomSoFar.incrementMode(mode3);
		      }
		    } // mode3
		  } // s3
		  
		  /// end of from level 3
		  
		  
		} // momSoFar is sensible
		if (s2) {
		  theMomSoFar.decrementMode(mode2);
		} else {
		  theMomSoFar.incrementMode(mode2);
		}
	      } // mode2
	    } // s2
	    

            //////// end of level 2 /////////

	  
	  } // momSoFar is sensible
	  if (s1) {
	    theMomSoFar.decrementMode(mode1);
	  } else {
	    theMomSoFar.incrementMode(mode1);
	  }
	} // mode1
      } // s1
	  

      /// end of level 1 //////

	 


    } // if there are interactions

  } // loop over pIndeces    
	

  m_found = globalFound;
  std::cout << "Found " << m_found << " in total, from " << m_contribs << " contribs." << std::endl;

} // setupMatrixelt func NEW
 




