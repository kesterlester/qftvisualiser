#ifndef LESTER_COEFFS_H
#define LESTER_COEFFS_H

#include "Workspace.h" // really only need Ket.h ?
#include "DerivativeMatrix.h"
#include "Ket.h"
#include <fftw3.h>


template<typename T>
class Coeffs {
private:
public:
  typedef typename DerivativeMatrix::MatrixElts MatrixElts;
  ~Coeffs() {
    freeFFT();
  }
  Coeffs(const Ket<T> & initialKet, const DerivativeMatrix & dm) : 
    m_ketNow(initialKet),
    m_ketTmp(),
    m_ketNext(),
    m_derivativeMatrix(dm),
    m_t(0),
    m_nextTToPrint(0) {
    allocFFT();
    clearCache();
  };
    
  Comp slow_getCoeff(const ModState & ms) const {
    const int index = Workspace::modStateToIndex(ms);
    if (index>=0 && index<Workspace::size()) {
      return m_ketNow[index];
    } else {
      return Comp(0,0);
    }
    /*
    const Workspace::ModStateMap::const_iterator it = Workspace::modStateToIndexMap().find(ms);


    if (it!=Workspace::modStateToIndexMap().end()) {
      return m_ketNow[it->second];
    } else {
      return Comp(0,0);
    }
    */
  }
  std::ostream & printTo(std::ostream & os) const {
    return os << "Coeffs[]";
  }

public:

 void update() {
    //std::cout << "." << std::flush;
    enum ALG {
      QFT, pureRot, fakeRot, translation
    };

    const ALG alg=QFT;

    //int m_normNext = 0;

    if (alg==QFT) {
      // QFT! 
      const Real dt=dtQFT;
      m_t += dt;

      if (m_t>m_nextTToPrint) {
	const Real nn = m_ketNow.computeNormNow();
	static const Real initialNorm = nn;
	
	//std::cout << "nn " << nn << " in " << initialNorm << std::endl;

	compareNorms();

	EnergyCache cache;
	const Real energyOfQuaterMode = cache[N/4];

	std::cout << "t=" << m_t << "\trelNorm=" << nn/initialNorm << "\tgrowthRate=" << (nn-initialNorm)/m_t*100/initialNorm << "%/s phi^" << PHIPOWER << " theory lambda="<<lambda<<" absNorm="<<nn /*<< " biggestMidNorm " << biggestMidNorm */<< " quaterModeEnergy " <<energyOfQuaterMode << " " << pow(energyOfQuaterMode*2,2) /*<< " norm1P " << norm1P */<< std::endl; 
	m_nextTToPrint += tPrintInterval;
	m_absError = nn-initialNorm;
      }

      ///// START EULER METHOD ////
      if ("EulerMethod" && false) {
	
	/*
	for (MatrixElts::const_iterator it = matrixElts.begin();
	     it != matrixElts.end();
	     ++it) {
	  Comp & coeffNextP = *(it -> coeffNextP);
	  Comp & coeffNowP = *(it -> coeffNowP);
	  coeffNextP = 0;
	  
	  const MatrixEltFor::Things & things = it -> things;
	  for (MatrixEltFor::Things::const_iterator it2 = things.begin();
	       it2 != things.end();
	       ++it2) {
	    coeffNextP += (it2->factor)*(*(it2->coeffNowQ));
	  }
	  coeffNextP *= minusI*dt;
	  coeffNextP += coeffNowP;
	  
	  m_normNext += norm(coeffNextP);
	  
	} // it
	
	*/

      } 
      ///// STOP EULER METHOD ////

      ///// START MIDPOINT METHOD ////
      else if ("MidpointMethod" && true) {
       
	const Real dtOnTwo = dt*0.5;

	const int size = Workspace::size();

	for (int pIndex=0; pIndex<size; ++pIndex) {

	  //Comp & coeffNextP(m_ketNext[pIndex]);
	  Comp & coeffTmp1P(m_ketTmp[pIndex]);
	  Comp & coeffNowP (m_ketNow[pIndex]);
	  coeffTmp1P = 0;
	  
	  const MatrixEltFor::Things & things = m_derivativeMatrix.matrixElts[pIndex].things;

	  for (MatrixEltFor::Things::const_iterator it2 = things.begin();
	       it2 != things.end();
	       ++it2) {
	    coeffTmp1P += (m_ketNow[it2->index])*(it2->factor);
	  }
	  coeffTmp1P *= minusI*dtOnTwo;
	  coeffTmp1P += coeffNowP;

	} // pIndex
	
	// by this point the coeffTmp1P hold the posn of the mid-point --
	// --  and we now use this point to evaluate the velocity to do the "real" step

	for (int pIndex=0; pIndex<size; ++pIndex) {

	  Comp & coeffNextP(m_ketNext[pIndex]);
	  Comp & coeffTmp1P(m_ketTmp[pIndex]);
	  Comp & coeffNowP(m_ketNow[pIndex]);
	  coeffNextP = 0;
	  
	  const MatrixEltFor::Things & things = m_derivativeMatrix.matrixElts[pIndex].things;

	  for (MatrixEltFor::Things::const_iterator it2 = things.begin();
	       it2 != things.end();
	       ++it2) {
	    coeffNextP += (m_ketTmp[it2->index])*(it2->factor);
	  }
	  coeffNextP *= minusI*dt;
	  coeffNextP += coeffNowP;

	  //m_normNext += norm(coeffNextP);

	} // it

      } 
      ///// STOP MIDPOINT METHOD ////


    } else if (alg==fakeRot) {
      /*
      // use this to test if the dt intagration can mimic pureRot

      const Real dt=0.05;
     
      for(CoeffMap::iterator it = m_coeffs.begin();
	  it!=m_coeffs.end();
	  ++it) {

	const PVecHash h = it->first;
	const Real energy = it->second.energy;
	const Comp coeffNow = it->second.now;
	

	const Comp derivative = minusI*energy*coeffNow;
	
	it->second.next = coeffNow + dt*derivative;

	m_normNext += norm(it->second.next);

      }
      */
    } else if (alg==translation) {
      /*
      // translation
      CoeffMap::iterator itNext; 
      for(CoeffMap::iterator it = m_coeffs.begin();
	  it!=m_coeffs.end();
	  ++it) {
	itNext = it;
	++itNext;
	if (itNext != m_coeffs.end()) {
	  itNext->second.next = it->second.now;
	} else {
	  m_coeffs.begin()->second.next = it->second.now;
	}
      }
      m_normNext = m_normNow;
      */
    } else if (alg==pureRot) {
      /*
      // phase rotation
      static const Real dt=0.1;
      //static const Comp rot(cos(theta),sin(theta));
      for(CoeffMap::iterator it = m_coeffs.begin();
	  it!=m_coeffs.end();
	  ++it) {
	const Real energy = it->second.energy;
	it->second.next =  exp(minusI*Comp(dt*energy))*(it->second.now);
      }
      m_normNext = m_normNow;
      */
    }

    /////////////////////////////////////////
    
    /////////////////////////////////////////
    // now move next to current:
    {
      m_ketNow.swap(m_ketNext);
    }

    clearCache();
  };
public:
  // data
  Ket<T> m_ketNow;
  Ket<T> m_ketTmp;
  Ket<T> m_ketNext;
  const DerivativeMatrix & m_derivativeMatrix;
  Real m_absError;
  Real m_t;
  Real m_nextTToPrint;
   
 private:
  bool iDont(bool & haveItAlready) {
    if (!haveItAlready) {
      haveItAlready = true;
      return true;
    } else {
      return false;
    }
  }
public:
  void clearCache() {
    have1DMomData=false;
    have1DPosData=false;
    have2DMomData=false;
    have2DPosData=false;
#ifdef CHECK_NORMS
    momNorm1D=0;
    posNorm1D=0;
    momNorm2D=0;
    posNorm2D=0;
#endif
  }
  
  // To correctly normalise the 1DMomData returned by this method, simply sum norms over all p.
  void get1DMomData() {
    if (iDont(have1DMomData)) {
      for (int p=0; p<N; ++p) {	
	ModState special;
	special.incrementMode(p);
	const Comp & coeff = slow_getCoeff(special);
	m_mom1D[p][0] = coeff.real();
	m_mom1D[p][1] = coeff.imag();
      }	
#ifdef CHECK_NORMS
      momNorm1D = 0;
      for (int p=0; p<N; ++p) {
	momNorm1D += norm(Comp(m_mom1D[p][0], m_mom1D[p][1])); 
      }
#endif
    }
  }

  // To correctly normalise the 2DMomData returned by this method, sum norms over all p<=q, but divide the diagonal (p==q) amplitided by sqrt(2) since they were increased by sqrt(2) for input to the FFT lib thingy.
  void get2DMomData() {
    if (iDont(have2DMomData)) {
#ifdef CHECK_NORMS
      momNorm2D = 0;
#endif
      
      // p and q are MODES
      for (int p=0; p<N; ++p) {
	for (int q=0; q<N; ++q) {	  
	  // see bayesian methods lab book, pages (70) and (78)
	  
	  ModState special;
	  special.incrementMode(p);
	  special.incrementMode(q);
	  const Comp & coeff = slow_getCoeff(special); 
	  
	  m_mom2D[p*N+q][0] = coeff.real();
	  m_mom2D[p*N+q][1] = coeff.imag();
	  
#ifdef CHECK_NORMS
	  if (p<=q){
	    momNorm2D += norm(Comp(m_mom2D[p*N+q][0], m_mom2D[p*N+q][1]));
	  }
#endif  
	}
	
	// now correct the diagonal entry: see bayesian methods lab book, pages (70) and (78)
	m_mom2D[p*N+p][0] *= sqrt(2.);
	m_mom2D[p*N+p][1] *= sqrt(2.);	
      }
    }
  }


  // To correctly normalise the 1DPosData returned by this method, it must be first divided by sqrt(N) at the amp level (or by N at the norm level) and then summed over all x.  We don't do the N divisions here, for speed.
  void get1DPosData() {
    if (iDont(have1DPosData)) {
      get1DMomData();
      fftw_execute(m_plan1D);      
      // now m_pos1D is filled
#ifdef CHECK_NORMS
      posNorm1D = 0;
      for (int x=0; x<N; ++x) {
	posNorm1D += norm(Comp(m_pos1D[x][0], m_pos1D[x][1])); 
      }
      posNorm1D /= (N);
#endif
    }
  }


  // To correctly normalise the 2DPosData returned by this method, it must be first divided by sqrt(N)*sqrt(N) at the amp level (or by N*N at the norm level) and then summed over all (x,y) s.t. x<=y.  We don't do the N divisions here, for speed.
  void get2DPosData() {
    if (iDont(have2DPosData)) {
      get2DMomData();
      fftw_execute(m_plan2D);
      // now m_pos2D is filled
      //Now correct diagonal entries (see bayesian methods lab book, pages (70) and (78))
      for (int x=0; x<N; ++x) {
	m_pos2D[x*N+x][0] /= sqrt(2.0);
	m_pos2D[x*N+x][1] /= sqrt(2.0);
      }
#ifdef CHECK_NORMS
      posNorm2D = 0;
      for (int x=0; x<N; ++x) {
	for (int y=0; y<=x; ++y) {
	  posNorm2D += norm(Comp(m_pos2D[x*N+y][0], m_pos2D[x*N+y][1]));
	}
      }
      posNorm2D /= (N*N);
#endif
    }
  }
 public:
  void compareNorms() {
#ifdef CHECK_NORMS
    get1DPosData();
    get1DMomData();
    get2DPosData();
    get2DMomData();
    std::cout 
 << "delta1D " << (posNorm1D - momNorm1D)/momNorm1D*100 << "% ratio1D " << posNorm1D/momNorm1D << " momNorm1D " << momNorm1D << " " 
 << "delta2D " << (posNorm2D - momNorm2D)/momNorm2D*100 << "% ratio2D " << posNorm2D/momNorm2D << " momNorm2D " << momNorm2D << " " 
 << std::endl;
#endif
  }

 private:
  void allocFFT() {
    m_mom1D = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    m_pos1D = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    m_mom2D = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N*N);
    m_pos2D = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N*N);
    
    if (!m_mom1D || !m_pos1D || !m_mom2D || !m_pos2D) {
      throw "Could not allocate fftw workspace";
    }   
    
    m_plan1D = fftw_plan_dft_1d(N,    m_mom1D, m_pos1D, FFTW_FORWARD, FFTW_ESTIMATE);
    m_plan2D = fftw_plan_dft_2d(N, N, m_mom2D, m_pos2D, FFTW_FORWARD, FFTW_ESTIMATE);
  }
  void freeFFT() {
    fftw_destroy_plan(m_plan1D);
    fftw_destroy_plan(m_plan2D);
    fftw_free(m_mom1D );
    fftw_free(m_pos1D);
    fftw_free(m_mom2D );
    fftw_free(m_pos2D);
  }
 public:
  fftw_complex * m_mom1D;
  fftw_complex * m_pos1D; 
  fftw_complex * m_mom2D;
  fftw_complex * m_pos2D; 
 private:
  bool have1DMomData;
  bool have1DPosData;
  bool have2DMomData;
  bool have2DPosData;
  fftw_plan m_plan1D;
  fftw_plan m_plan2D;

 public:
#ifdef CHECK_NORMS
    Real momNorm1D;
    Real posNorm1D;
    Real momNorm2D;
    Real posNorm2D;
#endif


private:
};

template<typename T>
std::ostream & operator<<(std::ostream & os, const Coeffs<T> & c) {
  return c.printTo(os);
}


#endif
