#ifndef LESTER_MATRIXEXTFOR_H
#define LESTER_MATRIXEXTFOR_H

#include <vector>
#include "Types.h"
#include "utils.h"
#include "globals.h"
#include <iostream>

struct MatrixEltFor {
  MatrixEltFor(/*int rowIndex*/) /*: m_rowIndex(rowIndex)*/ {
    // don't actually need to store rowIndex!
    things.reserve(2);
  }
  struct Thing {
    int index;
#ifdef REALMAT
    typedef Real FactorType;
#else
    typedef Comp FactorType;
#endif
    FactorType factor;
    std::ostream & dumpTo(std::ostream & os) const {
      os << "Type Thing "
	 << "Version 1 "
	 << "index " << index << " "
#ifdef REALMAT
	 << "factor " << factor << " " << 0;
#else
	 << "factor " << factor.real() << " " << factor.imag();
#endif
      return os;
    }
    std::istream & readFrom(std::istream & is) {
      YUMS("Type");
      YUMS("Thing");
      YUMS("Version");
      YUMS("1");
      YUMS("index");
      is >> index;
      YUMS("factor");
      Real re;
      Real im;
#ifdef REALMAT
      is >> re >> im;
      factor = re;
#else
      is >> re >> im;
      factor = Comp(re,im);
#endif
      return is;
    }
    bool operator==(const Thing & other) const {
	return index==other.index && factor==other.factor;
    }
    bool operator!=(const Thing & other) const {
	return index!=other.index || factor!=other.factor;
    }
    Real compareTo(const Thing & other) const {
      if (index != other.index) {
	std::cout << "Incomparable at " << __LINE__ << " of " << __FILE__ << std::endl;
	throw "Incomparable";
      }
      //const Real tot = norm(factor)+norm(other.factor);
      
      return fabs(norm(Comp(factor))-norm(Comp(other.factor))); // FIXME - WASTEFUL Comps
      //}
      //return fmin(    );
    }
    bool operator<(const Thing & other) const /* NOTE THIS ONLY PAYS ATTENTION TO INDEX! */ {
      return index < other.index;
    }
  };
  typedef std::vector<Thing> Things;

  /// DATA ///////////////////


  Things things;
  //int m_rowIndex;


  ////////////////////

  public:
  void takeNoteOf(const Thing & thing) {
    // try to incorporate into an existing factor if possible
    for (Things::iterator it = things.begin();
    	 it != things.end();
	 ++it) {
      if (it->index == thing.index) {
	it->factor += thing.factor;
	return;
      }
    }
    // ok .. couldn't combine -- need a new entry
    things.push_back(thing);
    ++globalFound;
  }
  void sortMyThings() {
    std::sort(things.begin(), things.end());
  }
  bool operator!=(const MatrixEltFor & other) const {
	return things != other.things;
  }
  bool operator==(const MatrixEltFor & other) const {
	return things == other.things;
  }
  Real compareTo(const MatrixEltFor & other) const {
    //if (m_rowIndex != other.m_rowIndex) {
    //  std::cout << "Incomparable at " << __LINE__ << " of " << __FILE__ << std::endl;
    //  throw "Incomparable";
   // }
    Real biggestDiff = 0;
    Things::const_iterator it1 = things.begin();
    Things::const_iterator it2 = other.things.begin();
    for (;it1!=things.end(); (++it1, ++it2)) {
      const Real diff = it1->compareTo(*it2);
      if (diff>biggestDiff) {
	biggestDiff = diff;
      }
    }
    return biggestDiff;
  }
  std::ostream & dumpTo(std::ostream & os) const {
    os << "Type MatrixEltFor "
	<< "Version 1 "
	<< "things.size " << things.size() << "\n";
    for (Things::const_iterator it = things.begin();
	it != things.end();
	++it) {
      it->dumpTo(os);
      os << "\n";
    }
    return os;
  }
  std::istream & readFrom(std::istream & is) {
    YUMS("Type");
    YUMS("MatrixEltFor");
    YUMS("Version");
    YUMS("1");
    YUMS("things.size");
    long int size;
    is >> size;
    things.clear();
    things.reserve(size);
    for (int i=0; i<size; ++i) {
      Thing thing;
      thing.readFrom(is);
      things.push_back(thing);
    } 
    return is;
  }

};

#endif
