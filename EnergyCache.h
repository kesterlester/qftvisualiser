#ifndef LESTER_ENERGYCACHE_H
#define LESTER_ENERGYCACHE_H

#include <vector>
#include "Types.h"
#include "config.h"
#include "globals.h"

struct EnergyCache {
public:
  EnergyCache() : mode(N) {
    for (int i=0; i<N; ++i) {
      mode[i]=energyOfMode(i);
    }
  }
private:
  static Real energyOfMode(int i) {
    const double mu=mass*epsilon;
    const double muSq=mu*mu;
    const double s = sin((lesterPi*i)/N);
    return sqrt(muSq + 4.*s*s)/epsilon;
  }
private:
  std::vector<Real> mode;
public:
  const Real & operator[](int i) const { return mode[i]; }
};

#endif
