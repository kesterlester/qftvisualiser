
#ifndef LESTER_KET_H
#define LESTER_KET_H

#include <vector>
#include "utils.h" // For lrand in the initialisation
#include "Workspace.h"
#include "globals.h"

template<typename BasisStateType>
class Ket {
private:
  typedef std::vector<Comp> CoeffVec;
public:
  void swap(Ket<BasisStateType> & other) {
    c.swap(other.c);
  }
  Ket() : c(Workspace::getInstance().size()) { 
  }
  explicit Ket(int theStartType) : c(Workspace::size(), Comp(0,0) ) {
    
#define BEGIN_LOOP					     \
    {							     \
      ModState state;					     \
      for (CoeffVec::iterator it = c.begin();		     \
	   it != c.end();				     \
	   ++it) {				    	     \
	Real lenn=0;					     \
	Real theta=0;
    
#define END_LOOP				             \
    *it = Comp(sin(theta),cos(theta))*Comp(lenn);	     \
    ++state;						     \
  }							     \
  }
    
    
    if (theStartType==0) {

      // random

      BEGIN_LOOP;
      lenn = lrand();
      theta = lrand()*2*lesterPi;
      END_LOOP;
      
    } else if (theStartType ==1) {

      // uniform 1 particle
      
      Real tot=0;
      for (int i=0; i<=globalMaxParticles; ++i) {
	tot += pow(N,i)/factorial(i);
      }
      BEGIN_LOOP;
      lenn=sqrt(1./tot);
      theta=0;
      END_LOOP;
      
    } else if (theStartType==2) {

      // just vacuum

      BEGIN_LOOP;
      lenn = (state.getNumParticles() == 0) ? 1 : 0;
      theta = 0;
      END_LOOP;
      
    } else if (theStartType==9) {
      
      // Plane wave

      ModState special;
      int place = std::min(N/4, 8);
      special.incrementMode((place)%N);
      //special.incrementMode((N-place)%N+1);

      BEGIN_LOOP;
      lenn = (state == special) ? 1 : 0;
      theta = 0;
      END_LOOP;
      
    } else if (theStartType==10) {
      
      // Plane waves clashing

      ModState special;
      int place = std::min(N/4, 8);
      special.incrementMode((place)%N);
      special.incrementMode((N-place)%N);

      BEGIN_LOOP;
      lenn = (state == special) ? 1 : 0;
      theta = 0;
      END_LOOP;
      
    } else if (theStartType==3 or theStartType==8) {
      
      ModState special;
      special.incrementMode(startType==3 ? 0 : static_cast<int>(1.*N/4.));
      BEGIN_LOOP;
      lenn = (state==special ? 1 : 0);
      theta = 0;
      END_LOOP;
      
    } else if (theStartType==4) {
      
      ModState special;
      special.incrementMode(0);
      BEGIN_LOOP;
      lenn = (state == special ||  state.getNumParticles()==0 ) ? 1./sqrt(2.) : 0;
      theta = 0;
      END_LOOP;
      
    } else if (theStartType==5) {
      
      BEGIN_LOOP;
      if (state.getNumParticles()==1) {
	//static Real th = 0;
	lenn=sqrt(1./N);

	int rainbow=0;
	{
	  for (int i=0; i<N; ++i) {
	    rainbow += i*state.getOccupancyOfMode(i);
	  }
	}

	theta=-lesterPi*rainbow; //lesterPi;
	//th += 0.4*20./N;
      } else {
	lenn = 0;
	theta = 0;
      }
      END_LOOP;
      
    } else if (theStartType==6) {
      
      const double sigma=1.*N/8.;
      const double meanP=1.*N/4;
      const double xPos = 1.*N-sigma*2;

      // calc norm factor:
      Real norm=0;
      for (Real pos=0; pos<N; ++pos) {
	const Real amp = 1./sqrt(2*lesterPi*sigma*sigma)*exp(-(pos-meanP)*(pos-meanP)/(2.*sigma*sigma));
	norm+=amp*amp;
      }
      
      BEGIN_LOOP;
      if (state.getNumParticles()==1) {
	
	
	//static Real th = 0;
	
	int rainbow=0;
	{
	  for (int i=0; i<N; ++i) {
	    rainbow += i*state.getOccupancyOfMode(i);
	  }
	}
	const int pos = rainbow;

	lenn=1./sqrt(2*lesterPi*sigma*sigma)*exp(-(pos-meanP)*(pos-meanP)/(2.*sigma*sigma))/sqrt(norm);
	theta=-2.*lesterPi*pos*xPos/N;
	//th += 0.4*20./N;
      } else {
	lenn = 0;
	theta = 0;
      }
      END_LOOP;
      
    } else if (theStartType==7) {
      
      // calc norm factor:
      Real norm=0;
      for (Real x=0; x<N; ++x) {
	for (Real y=0; y<N; ++y) {
	  const Real amp = 
	    1./sqrt(2*lesterPi*BLOBsigma*BLOBsigma)*exp(-mod(x-BLOBmeanPX,N)*mod(x-BLOBmeanPX,N)/(2.*BLOBsigma*BLOBsigma))
	    *
	    1./sqrt(2*lesterPi*BLOBsigma*BLOBsigma)*exp(-mod(y-BLOBmeanPY,N)*mod(y-BLOBmeanPY,N)/(2.*BLOBsigma*BLOBsigma));
	  norm+=amp*amp;
	}
      }
      
      BEGIN_LOOP;
      if (state.getNumParticles()==2) {
       
	int x=0;
	int y=0;
	bool foundx=false;
	bool foundy=false;
	for (int i=0; i<N && !(foundx && foundy); ++i) {
	  if (state.getOccupancyOfMode(i)==0) {
	  } else if (state.getOccupancyOfMode(i)==1) {
	    // it's just a 1
	    if (!foundx) {
	      x=i;
	      foundx=true;
	    } else {
	      y=i;
	      foundy=true;
	    }
	  } else {
	    // its a two
	    y=x=i;
	    foundx=foundy=true;
	  }
	}
	
	
	lenn =
	  1./sqrt(2*lesterPi*BLOBsigma*BLOBsigma)*exp(-mod(x-BLOBmeanPX,N)*mod(x-BLOBmeanPX,N)/(2.*BLOBsigma*BLOBsigma))
	  *
	  1./sqrt(2*lesterPi*BLOBsigma*BLOBsigma)*exp(-mod(y-BLOBmeanPY,N)*mod(y-BLOBmeanPY,N)/(2.*BLOBsigma*BLOBsigma))
	  /sqrt(norm);
	
	theta=-2.*lesterPi*(x*BLOBxPos+y*BLOByPos)/N;
	
	//std::cout << "x " << x << " y " << y << " xp " << BLOBxPos << " yp " << BLOByPos << " lenn " << lenn << " theta " << theta << std::endl;
	//th += 0.4*20./N;
      } else {
	lenn = 0;
	theta = 0;
      }
      END_LOOP;
    }
    
    computeNormNow();
  }
  Real computeNormNow() const {
    m_norm=0;
    for (CoeffVec::const_iterator it=c.begin();
	 it != c.end();
	 ++it) {
      m_norm += norm(*it);
    }
    return m_norm;
  }
  Real lastComputedNorm() const {
    return m_norm;
  }
  std::ostream & printTo(std::ostream & os) const {
    os << "Ket[";
    std::copy(c.begin(), c.end(), std::ostream_iterator<Comp>(os,","));
    os << "]";
    return os;
  }
  inline const Comp & operator[](const int index) const {
    return c[index];
  }
  inline Comp & operator[](const int index) {
    return c[index];
  }
private:
  CoeffVec c; // the coeffs of the state
  mutable Real m_norm;     

};

template <typename T>
std::ostream & operator<<(std::ostream & os, const Ket<T> & ket) {
  return ket.printTo(os);
}

#endif
