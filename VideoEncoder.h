#ifndef LESTER_VIDEOECODER_H
#define LESTER_VIDEOECODER_H

/*
 * Copyright (c) 2001 Fabrice Bellard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @file
 * libavcodec API use example.
 *
 * Note that libavcodec only handles codecs (mpeg, mpeg4, etc...),
 * not file formats (avi, vob, mp4, mov, mkv, mxf, flv, mpegts, mpegps, etc...). See library 'libavformat' for the
 * format handling
 */

// NEED THE FOLLOWING TWO LINES FOR UINT64_C ... HOWEVER SINCE MUST COME BEFORE FIRST CALL TO #include <stdint.h>, ... we must put them in the compiler flags:

// #define __STDC_CONSTANT_MACROS
// #include <stdint.h>

#ifdef LESTER_VIDEO

extern "C" {
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
}

#include <cmath>
#include "Coeffs.h"
#include "displayUtils.h"
 
#endif

/*
 * Video encoding example
 */


class VideoEncoder {
#ifdef LESTER_VIDEO
 public:
  CodecID codec_id;
  const char * filename;
  AVCodecContext *c;
  int outbuf_size, out_size;
  uint8_t *outbuf;
  FILE *f;
  AVFrame *picture;
  int had_output;
  int i;
public:
  VideoEncoder(const char *fileName, const CodecID CODEC_ID = CODEC_ID_MPEG1VIDEO) : had_output(0), c(NULL), filename(fileName), codec_id(CODEC_ID), i(0) {
    // OTHERS CODEC_ID_MPEG1VIDEO;
    // OTHERS CODEC_ID_H264;

  }

  void start() {

    avcodec_register_all();

    AVCodec *codec = avcodec_find_encoder(codec_id);

    printf("Encoding video file %s\n", filename);

    /* find the mpeg1 video encoder */
    codec = avcodec_find_encoder(codec_id);
    if (!codec) {
      fprintf(stderr, "codec not found\n");
      exit(1);
    }

    c = avcodec_alloc_context3(codec);
    picture= avcodec_alloc_frame();

    /* put sample parameters */
    c->bit_rate = 400000;
    /* resolution must be a multiple of two */
    c->width = 352;
    c->height = 288;
    /* frames per second */
    c->time_base= (AVRational){1,25};
    c->gop_size = 10; /* emit one intra frame every ten frames */
    c->max_b_frames=1;
    c->pix_fmt = PIX_FMT_YUV420P;
    //c->pix_fmt = PIX_FMT_YUV444P;

    if(codec_id == CODEC_ID_H264)
      av_opt_set(c->priv_data, "preset", "slow", 0);

    /* open it */
    if (avcodec_open2(c, codec, NULL) < 0) {
      fprintf(stderr, "could not open codec\n");
      exit(1);
    }

    f = fopen(filename, "wb");
    if (!f) {
      fprintf(stderr, "could not open %s\n", filename);
      exit(1);
    }

    /* alloc image and output buffer */
    outbuf_size = 100000 + 12*c->width*c->height;
    outbuf = static_cast<uint8_t*>(malloc(outbuf_size));

    /* the image can be allocated by any means and av_image_alloc() is
     * just the most convenient way if av_malloc() is to be used */
    av_image_alloc(picture->data, picture->linesize,
                   c->width, c->height, c->pix_fmt, 1);

  }

  void encode2DPOS(Coeffs<ModState> & coeffs) {
    coeffs.get2DPosData();
    for (int x=0; x<height; ++x) {
      for (int y=0; y<width; ++y) {
	Comp  z0=Comp(coeffs.m_pos2D[x*N+y][0],coeffs.m_pos2D[x*N+y][1])/sqrt(static_cast<Real>(N))/sqrt(static_cast<Real>(N));	  
	picture->data[0][y * picture->linesize[0] + x] = phaseInt(z0);
      }
    }
    for (int x=0; x<height/2; ++x) {
      for (int y=0; y<width/2; ++y) {
	picture->data[1][y * picture->linesize[1] + x] = 128;
	picture->data[2][y * picture->linesize[2] + x] = 128;
      }
    }
  }

  void testEncodeSomeFrames2() {


    /*	 THis example generates a "still" video supposed to resemble

    http://en.wikipedia.org/wiki/File:YCbCr-CbCr_Scaled_Y50.png

    From the example we conclude that the PIXEL DOWNSIZING of Cb and Cr happens at the level of the picture itself	(note that the ranges over which x and y loop is different for Y versus Cb and Cr) and all numbers are 0:255 ... with Y:   0=black 255=saturate,
    Cb: 0=(Cb:-1), 255=(Cb:+1)
    Cr: 0=(Cr:-1), 255=(Cr:+1)
    */

    /* encode 1 second of video */
    for(i=0;i<75;i++) {

      fflush(stdout);
      /* prepare a dummy image */
      /* Y */
      for(int y=0;y<c->height;y++) {
	for(int x=0;x<c->width;x++) {
	  picture->data[0][y * picture->linesize[0] + x] = 128+0*static_cast<unsigned int>(256*pow(sin(2*3.141*sqrt(x*x+y*y)/(c->width)),2))%256 ;
	}
      }

      /* Cb and Cr */
      for(int y=0;y<c->height/2;y++) {
	for(int x=0;x<c->width/2;x++) {

	  // the x and y have an origin at "top left".
	      
	  // Try to resemble http://en.wikipedia.org/wiki/YCbCr picture
	  const float ermX /*0 at leftEdge, +1 at rightEdge */ = (1.0*x/(c->width/2));
	  const float ermY /*0 at botEdge, +1 at topEdge */ = 1.0-(1.0*y/(c->height/2));

	  picture->data[1][y * picture->linesize[1] + x] = static_cast<int>(ermX*256); // Cb
	  picture->data[2][y * picture->linesize[2] + x] = static_cast<int>(ermY*256); // Cr
	}
      }

      /* encode the image */
      out_size = avcodec_encode_video(c, outbuf, outbuf_size, picture);
      had_output |= out_size;
      printf("encoding frame %3d (size=%5d)\n", i, out_size);
      fwrite(outbuf, 1, out_size, f);
    }

  }
  void testEncodeSomeFrames() {
    /* encode 1 second of video */
    for(i=0;i<25;i++) {

      fflush(stdout);
      /* prepare a dummy image */
      /* Y */
      for(int y=0;y<c->height;y++) {
	for(int x=0;x<c->width;x++) {
	  picture->data[0][y * picture->linesize[0] + x] = x + y + i * 3;
	}
      }

      /* Cb and Cr */
      for(int y=0;y<c->height/2;y++) {
	for(int x=0;x<c->width/2;x++) {
	  picture->data[1][y * picture->linesize[1] + x] = 128 + y + i * 2;
	  picture->data[2][y * picture->linesize[2] + x] = 64 + x + i * 5;
	}
      }

      /* encode the image */
      out_size = avcodec_encode_video(c, outbuf, outbuf_size, picture);
      had_output |= out_size;
      printf("encoding frame %3d (size=%5d)\n", i, out_size);
      fwrite(outbuf, 1, out_size, f);
    }

  }

  void stop() {
    /* get the delayed frames */
    for(; out_size || !had_output; i++) {
      fflush(stdout);

      out_size = avcodec_encode_video(c, outbuf, outbuf_size, NULL);
      had_output |= out_size;
      printf("write frame %3d (size=%5d)\n", i, out_size);
      fwrite(outbuf, 1, out_size, f);
    }

    /* add sequence end code to have a real mpeg file */
    outbuf[0] = 0x00;
    outbuf[1] = 0x00;
    outbuf[2] = 0x01;
    outbuf[3] = 0xb7;
    fwrite(outbuf, 1, 4, f);
    fclose(f);
    free(outbuf);

    avcodec_close(c);
    av_free(c);
    av_free(picture->data[0]);
    av_free(picture);
    printf("\n");
  }

#endif
};

#endif
