    if (m_tot != other.m_tot) {
      // they have different number of particles
      return true;
    }
    // both have the same number of particles
    if (m_tot==0) {
      // both are vacuum
      return false;
    }
    for (int i=0; i<N; ++i) {
      if (m_occ[i] != other.m_occ[i]) {
	// they are different
	return true;
      }
    }
    // we couldn't find a difference, so they are identical
    return false;
